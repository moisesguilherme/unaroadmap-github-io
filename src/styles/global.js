import { createGlobalStyle } from 'styled-components';

import "font-awesome/css/font-awesome.css";

export const GlobalStyle = createGlobalStyle`
    :root{
    --main-red: rgb(211,9,19);
    --main-red-hover: #e50914;
    --main-dark: rgb(20,20,20);
    --main-deep-dark: rgb(0,0,0);
    --main-grey: rgba(153,153,153);
    --main-white: rgba(255, 255, 255);
    --main-dark-blue: rgb(11, 33, 45);
}

     * {
        box-sizing: border-box;
        padding: 0;
        margin: 0;
        outline: 0;
    } 
    body, html {
        background: #ffff;
        font-family: 'Roboto','Helvetica Neue', 'Helvetica', Arial, sans-serif;
        text-rendering: optimizeLegibility !important;
        -webkit-font-smoothing: antialiased !important;
        height: 100%;
        width: 100%;
        color: #000;
    }

`;
