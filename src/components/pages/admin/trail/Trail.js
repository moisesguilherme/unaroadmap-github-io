import React, {Component} from 'react';
import BaseUrl from '../../../../routes/BaseUrl';
import axios from 'axios';

import Main from '../template-admin/Main/Main';
import './../Admin.css';

import Logo from '../template-admin/Logo/Logo';
import Nav from '../template-admin/Nav/Nav';
import Footer from '../template-admin/Footer/Footer';

const BaseUrlTrail = BaseUrl+'trails/';

const headerProps = {
    icon: 'road',
    title: 'Trilhas',
    subtitle: 'Cadastro de Trilhas: Inserir, Listar, Alterar e Excluir'
}

const inicial = {
    trail: { name: '', description: ''},
    list: []
}

export default class Trail extends Component {

  state = { ...inicial };

  componentWillMount() {
      axios(BaseUrlTrail).then(resp => {
          this.setState({ list: resp.data });
      })
  }

  limpar() {
      this.setState({ trail: inicial.trail });
  }

  criar() {
    const trail = this.state.trail
    const method = trail.id != null ? 'put' : 'post'
    const url = trail.id != null ? `${BaseUrlTrail}${trail.id}` : BaseUrlTrail
    axios[method](url, trail)
        .then(resp => {
            const list = this.atualizarLista(resp.data)
            this.setState({ trail: inicial.trail, list })
        })
    }

  atualizarLista(trail, add = true) {
    const list = this.state.list.filter(u => u.id !== trail.id);
    if(add) list.unshift(trail);
    return list;
  }

  atualizarCampos(e) {
      const trail = { ...this.state.trail };
      trail[e.target.name] = e.target.value;

      this.setState({trail: trail});
  }
  
  renderForm() {
        return (
            <div className="form">
                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label>Nome</label>
                            <input type="text" className="form-control"
                                name="name"
                                value={this.state.trail.name}
                                onChange={e => this.atualizarCampos(e)}
                                placeholder="Digite o nome..." />
                        </div>
                    </div>

                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label>Descrição</label>
                            <input type="text" className="form-control"
                                name="description"
                                value={this.state.trail.description}
                                onChange={e => this.atualizarCampos(e)}
                                placeholder="Digite a descrição..." />
                        </div>
                    </div>
                </div>

                <hr />
                <div className="row">
                    <div className="col-12 d-flex justify-content-end">
                        <button className="btn btn-outline-primary"
                            onClick={e => this.criar(e)}>
                            Salvar
                        </button>

                        <button className="btn btn-outline-secondary ml-2"
                            onClick={e => this.limpar(e)}>
                            Limpar
                        </button>
                    </div>
                </div>
            </div>
        )
    }


  carregar(trail) {
      this.setState({trail: trail});
  }

  remover(trail) {
      axios.delete(`${BaseUrlTrail}${trail.id}`).then(resp => {
          const list = this.atualizarLista(trail, false);
          this.setState({ list: list});
      });
  }

  renderTable() {
    return (
        <table className="table mt-4">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Descrição</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                {this.renderRows()}
            </tbody>
        </table>
    )
}

renderRows() {
    return this.state.list.map(trail => {
        return (
            <tr key={trail.id}>
                <th scope="row">{trail.id}</th>
                <td>{trail.name}</td>
                <td>{trail.description}</td>
                <td>
                    <button className="btn btn-warning"
                        onClick={() => this.carregar(trail)}>
                        <i className="fa fa-pencil"></i>
                    </button>
                    <button className="btn btn-danger ml-2"
                        onClick={() => this.remover(trail)}>
                        <i className="fa fa-trash"></i>
                    </button>
                </td>
            </tr>
        )
    })
}

  render() {
      return (
        <React.Fragment>
          <div className="app">
            <Logo />
            <Nav />
            <Main {...headerProps}>
                {this.renderForm()}
                {this.renderTable()}
            </Main>
            <Footer />
          </div>
        </React.Fragment> 
      );
  }
}
