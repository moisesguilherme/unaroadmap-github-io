import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
    background: #000f22;
  }
`;

export const ContainerPrincipal = styled.div`
  .App {
    display: flex;
    padding: 8% 0 0;
    justify-content: center;
  }

  .special {
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
  }

  .form {
    background: #ffffff;
    margin: 0 auto 0px;
    padding: 45px;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
  }

  .logo {
    display: block;
    margin-left: auto;
    margin-right: auto;
  }

  .form button  {
    font-family: "Roboto", sans-serif;
    text-transform: uppercase;
    outline: 0;
    background: #e8304c;
    width: 100%;
    border: 0;
    padding: 15px;
    color: #ffffff;
    font-size: 14px;
    -webkit-transition: all 0.3 ease;
    transition: all 0.3 ease;
    cursor: pointer;
    font-weight: bold;
    letter-spacing: 1.5px;
  }
  .form button:hover,
  .form button:active,
  .form button:focus {
    background: #e31937;
  }

  .linkbar { 
    padding-top: 10px;
  }

  .linkBtn{
    text-decoration: none;
    color: #000000;
  }

  .justify-right{
    text-align: right;
  }

  .justify-left{
    text-align: left;
  }

  .term-line{
    justify-content: center;
    text-align: center;
  }
  
  .form input{
    font-family: "Roboto", sans-serif;
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    box-sizing: border-box;
    margin: 0 0 0px;
    padding: 8px;
    font-size: 14px;
  }

  .form input[type=checkbox] {
    background: #26ca28;
    width: 15px;
    height: 15px;
    padding-top: 5px;
  }


  .form select{
    background: #f2f2f2;  
  }

  .special {
    height: 37px;
  }
  
  .btnPesquisarCEP{  
  }
`;