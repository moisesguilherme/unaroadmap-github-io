import React, { useState } from "react";
import { Link, useHistory } from 'react-router-dom';
import {
  Container,
  Col,
  Row,
  FormGroup,
  Label,
  Input,
  Button,
  FormText,
} from "reactstrap";
import Logo from "./../../../assets/imgs/logo_roadmap_small.svg";
import { ContainerPrincipal, GlobalStyle } from './ComponentStyle';


import Form from 'react-bootstrap/Form'
import api from '../../../services/api';
import { login } from '../../../services/auth';
import { cpfMask, cnpjMask } from './util/FormMask'

//Redux dependecies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
//Necessário o Redux para salvar o id do usuário
import * as LoginActions from '../../../store/actions/_login'

function RegisterUser({ _login, loginWithRegister  }) {

  const MSG_EMAIL_DUPLICATE = 'Este email já foi cadastrado.';
  const MSG_INVALID_EMAIL = 'Email inválido.'
    
  //Change the inputs
  const [checked, setChecked] = useState(false);
  const [documentId, setDocumentId] = useState('')
   
  let user_id = null;

  const [data, setData] = useState({
    email: '',
    password: '',
    repeatPassword: '',
    profile: '',
    formErrors: {
      email: '',
      password: '',
      userTerm: ''
    }
  })

  const history = useHistory();

  const emailRegex = RegExp(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  )

  const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
      val.length > 0 && (valid = false);
    });

    // validate the form was filled out (null vazio)
    Object.values(rest).forEach(val => {
      val === null && (valid = false)
    });

    return valid;
  }

  function verifyFormOk() {
    let formOk = false;

    if ((data.formErrors.password === '' &&
      data.formErrors.email === '' &&
      data.password.length > 0 &&
      data.password === data.repeatPassword &&
      data.email.length > 0 &&
      checked === true &&
      documentId.length >= 14 && documentId.length <= 18
    )
    ) formOk = true;

    return formOk

  }

  async function handleSubmit(e) {
    e.preventDefault();

    //todo: isFormValid
    if (formValid(data)) {
      const userData = {
        email: data.email,
        password: data.password,
        profile: data.profile,
        status: 'Active',
      }

      createUser(userData).then( v=> {
          
          //Verifica se o email é dubplicado
          if(!data.formErrors.email.length){
            console.log("> #1 autentica usuário")
            authenticateUser(userData).then( v => {
                //ir para a página de candidato ou empresa
                const pageRedirect = data.profile === 'Candidato' ? 'candidate' : 'company';
                console.log("> #3 redireciona para:", pageRedirect);
                loginWithRegister(user_id, documentId); //Redux passa o CPF/CNPJ
                history.push(`/register/${pageRedirect}`);
          }); 
        }
   
     });

    } else {
      console.error('FORM INVALID - DISPLAY ERROR MESSAGE')
    }
  };


  function notificationEmailDuplicate(){
     let formErrors = Object.assign(data.formErrors);
        formErrors.email = MSG_EMAIL_DUPLICATE;
        setData({formErrors, ...data});
  }


  async function createUser(userData) {   
      const response = await api.post('users', userData).then((response) => {
        
        console.log(`
        --SALVOU--
        ${JSON.stringify(response.data)}
      `)

      user_id = response.data.id;

   }, (error) => {
     console.log(">>>", error);
     //TODO: precisa tratar o erro
     notificationEmailDuplicate();
   })
  }

  async function authenticateUser(userData){
      //Autenticação
      const { email, password } = userData;
      const responseLogin = await api.post("/users/login", { email, password });
      login(responseLogin.data.token);
  }

  function handleChange(e) {
    e.preventDefault();

    const { name, value } = e.target;
    let formErrors = Object.assign(data.formErrors);
    const userData = { ...data }

    switch (name) {
      case 'email':
        formErrors.email = emailRegex.test(value) && value.length > 0
          ? ''
          : 'Email inválido';
        break;
      case 'password':
        formErrors.password = value.length < 6 && value.length > 0
          ? 'Mínimo de 6 letras'
          : '';
        break;
      case 'documentId':
        updateDocumentId(value);
        userData.profile = typeOfUser(value);
        break;
      default:
        break;
    }
   
    userData[name] = value;
    setData({ formErrors, ...userData });
  }


  //Change input CPF/CNPJ 
  function updateDocumentId(strValue) {
    
     let newValue = (typeOfUser(strValue) === 'Candidato')
       ? cpfMask(strValue)
       : cnpjMask(strValue)
        
      setDocumentId(newValue);
  }

  // Return Candidato ou Empresa
  function typeOfUser(strValue) {
    let strType = 'Candidato'
    
    if ( (strValue.length > 14 && strValue.length <= 18)
        || strValue.length > 18) strType = "Empresa"

    return strType;
  }
  
  //Apenas para teste em produçao
  // function testPass(local){
  //   loginWithRegister(601, '047353676-59');
  //   history.push(`/register/${local}`);
  // }

  return (
    <React.Fragment>
      <GlobalStyle />
      <ContainerPrincipal>
        <Container fluid className="App">

          <Form className="form" onSubmit={(e) => handleSubmit(e)} noValidate>
  
          <Col>
              {/* { process.env.NODE_ENV === 'development'? <Button onClick={(e) => testPass('company')}>!DEBUG - EMPRESA</Button> : '' }
              { process.env.NODE_ENV === 'development'? <Button onClick={(e) => testPass('candidate')}>!DEBUG - CANDIDATO</Button> : '' } */}
              <img className="logo" src={Logo} alt="" /><br />
            </Col>
            <Col>
              <FormGroup>
                <Label for="documentId">CPF ou CNPJ</Label>
                <Input
                  name="documentId"
                  value={documentId}
                  noValidate
                  placeholder="Digite o seu CPF ou CNPJ"
                  onChange={(e) => handleChange(e)}
                />
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Label for="emailAdress">Endereço de email</Label>
                <Input
                  id="emailAdress"
                  type="email"
                  name="email"
                  noValidate
                  autoComplete="username"
                  placeholder="Digite o seu email..."
                  onChange={(e) => handleChange(e)}
                />
                {data.formErrors.email.length > 0 && (
                  <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.email}</span>
                )}
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Label htmlFor="password">Senha</Label>
                <Input
                  placeholder=""
                  type="password"
                  name="password"
                  id="password"
                  autoComplete="new-password"
                  noValidate
                  onChange={(e) => handleChange(e)}
                />
                {data.formErrors.password.length > 0 && (
                  <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.password}</span>
                )}
              </FormGroup>
            </Col>

            <Col>
              <FormGroup>
                <Label for="repeatPassword">Repita a Senha</Label>
                <Input type="password" 
                       name="repeatPassword" 
                       placeholder="" 
                       autoComplete="new-password"
                       onChange={(e) => handleChange(e)} />
                <FormText color="muted">
                  Nunca vamos compartilhar o seus dados.
                </FormText>
              </FormGroup>
            </Col>
            <Row>
              <Col className="term-line">
                <FormGroup>

                  <input type="checkbox"
                    checked={checked} onChange={(e) => setChecked(!checked)}
                  />
                  <Label>&nbsp;&nbsp;Aceito os termos de uso.</Label>

                </FormGroup>
              </Col>
              <Button type="submit" block disabled={!verifyFormOk()}>
                Criar conta
            </Button>
            </Row>
            <Row className="linkbar">
              <Col className="justify-left">
                <Link className="linkBtn" to="#" onClick={() => history.goBack()}>Voltar</Link>
              </Col>
              <Col className="justify-right">
                {/* <Link className="linkBtn" to="/register/user">Cadastrar</Link> */}
              </Col>
            </Row>

          </Form>
        </Container>
      </ContainerPrincipal>
    </React.Fragment >

  );
}

RegisterUser.prototype = {
  loginWithRegister: PropTypes.func.isRequired,
  _login: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    document_id: PropTypes.string,
  })).isRequired
}

const mapStateToProps = state => ({
  _login: state.login,
})


const mapDispatchToProps = dispatch => bindActionCreators(LoginActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RegisterUser);