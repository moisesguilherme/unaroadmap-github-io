import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import InputMask from 'react-input-mask';
import {
  Container,
  Col,
  Row,
  Button,
} from "reactstrap";

import Form from 'react-bootstrap/Form'

import Logo from "./../../../assets/imgs/logo_roadmap_small.svg";
import { ContainerPrincipal, GlobalStyle } from './ComponentStyle';

import api from '../../../services/api';
import Address from './FormAddress';

//Redux dependecies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as LoginActions from '../../../store/actions/_login'

function RegisterCompany({ _login, loginSuccess }) {

  const MSG_ERRO = 'Email inválido.'

  //name, cnpj, user_id, email, telephone, cell_phone
  const [formCompleted, setFormCompleted] = useState(false);
  const [data, setData] = useState({
    name: '',
    cnpj: '',
    user_id: '',
    email: '',
    telephone: '',
    cell_phone: '',
    address: null,
    formErrors: {
      name: '',
      email: '',
      telephone: '',
    }
  })

  const history = useHistory();
  let user_id = infoUser('id');
  let document_id = infoUser('document_id'); //CNPJ
  let company_id = null;

  console.log(">>> redux: ", user_id, document_id)

  const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
      val.length > 0 && (valid = false);
    });

    // validate the form was filled out (null vazio)
    Object.values(rest).forEach(val => {
      val === null && (valid = false)
    });

    return valid;
  }

  function infoUser(data) {
    let data_redux;

    try {
      data_redux = _login[0][data];
    } catch (error) {
      console.log(">>> REDUX, náo passo ou user_id");
      history.push('/');
    }

    return data_redux;
  }


  // function verifyFormOk() {
  //   let formOk = false;

  //   //Verifica a data
    
  //   if ( (data.formErrors.name === '' && data.name.length >= 3 ) &&
  //        (data.formErrors.telephone === '' && data.telephone.length > 0) &&
  //        (data.formErrors.email === '' && data.email.length > 0) &&
  //        data.address != null
  //   ) formOk = true;

  //   return formOk;
  // }

  async function handleSubmit(e) {
    e.preventDefault();

    if (formValid(data)) {

      const companyData = {
        name: data.name,
        cnpj: document_id,
        user_id: user_id,
        email: data.email,
        telephone: data.telephone,
        cell_phone: data.cell_phone
      }

      const addressData = {
        cep: data.address.cep,  
        logradouro: data.address.logradouro,
        complement: data.address.complement,
        number: Number(data.address.number),
        district_id: 1, //provisório
        user_id: user_id,
      }

      saveDataApi(companyData, "companys");
      saveDataApi(addressData,  "address");
      //passar o id no redux
      loginSuccess(user_id);
      history.push(`/admin/`);


    } else {
      console.error('FORM INVALID - DISPLAY ERROR MESSAGE')
    }
  };

  async function saveDataApi(userData, endpoint) {

    const response = await api.post(endpoint, userData).then((response) => {
      console.log(`
         --SALVOU--
         ${JSON.stringify(response.data)}
       `)

       return response;
    }, (error) => {
      console.log(">>> erro ", endpoint, error);
    });    
  }

  function handleChange(e) {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = Object.assign(data.formErrors);

    switch (name) {
      case 'name':
        formErrors.name = value.length >= 3
          ? ''
          : 'Favor preencher o nome';
        break;

      case 'telephone':
        formErrors.telephone = value !== ""
          ? ''
          : 'Favor preencher um número de telefone';
        break;

      case 'email':
        formErrors.email = validateEmail(e);
        break;
      default:
        break;
    }

    const userData = { ...data }
    userData[name] = value;
    setData({ formErrors, ...userData });
  }

  function validateEmail(e) {
    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let status = '';

    if (!emailRex.test(e.target.value)) {
      status = MSG_ERRO
    }
    return status;
  }

  function callbackDataAddress(address_data) {
    console.log("Endereço completo: OK", address_data)
    data.address = address_data;
    setFormCompleted(true);
  }


  return (
    <React.Fragment>
      <GlobalStyle />
      <ContainerPrincipal>
        <Container fluid className="App" >
          <Form className="form col-6" onSubmit={(e) => handleSubmit(e)} noValidate>
            <Row>
              {/* linha pincipal */}
              <Col>
                <img className="logo" src={Logo} alt="" />
                <center><span><strong>Cadastro de Empresa na Plataforma</strong></span></center>
                <br />
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Nome da empresa</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    onChange={(e) => handleChange(e)}
                  />
                  {data.formErrors.name.length > 0 && (
                    <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.name}</span>
                  )}

                </Form.Group>
              </Col>
            </Row>

            <Row>
              <Col className="col-8">
                <Form.Group>
                  <Form.Label>E-mail</Form.Label>
                  <Form.Control
                    type="text"
                    name="email"
                    onChange={(e) => { handleChange(e) }}
                  />
                  {data.formErrors.email.length > 0 && (
                    <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.email}</span>
                  )}
                </Form.Group>
              </Col>

            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Telefone</Form.Label>
                  <InputMask
                    name="telephone"
                    mask="+55\ (099) 99999-9999"
                    maskChar=" "
                    onChange={(e) => handleChange(e)}
                  />
                  {data.formErrors.telephone.length > 0 && (
                    <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.telephone}</span>
                  )}

                </Form.Group>
              </Col>
              <Col>
                <Form.Group>
                  <Form.Label>Celular</Form.Label>
                  <InputMask
                    name="cell_phone"
                    mask="+55\ (099) 99999-9999"
                    maskChar=" "
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>
              </Col>
            </Row>

            <Row>
              <Col>
                <Address data={callbackDataAddress} />
              </Col>
            </Row>

            <Row>
              <Col>
                <Button variant="primary" type="submit" block disabled={!formCompleted}>
                  Cadastrar Empresa
                </Button>
              </Col>
            </Row>

          </Form>
        </Container>
      </ContainerPrincipal>
    </React.Fragment >
  );
}


RegisterCompany.prototype = {
  loginSuccess: PropTypes.func.isRequired,
  _login: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    text: PropTypes.string,
  })).isRequired
}


const mapStateToProps = state => ({
  _login: state.login,
})

const mapDispatchToProps = dispatch => bindActionCreators(LoginActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RegisterCompany);