import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import InputMask from 'react-input-mask';

import {
  Container,
  Col,
  Row,
  Button,
} from "reactstrap";

import Form from 'react-bootstrap/Form'

import Logo from "./../../../assets/imgs/logo_roadmap_small.svg";
import { ContainerPrincipal, GlobalStyle } from './ComponentStyle';

import api from '../../../services/api';
import Address from './FormAddress';

//Redux dependecies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as LoginActions from '../../../store/actions/_login'

const RegisterCandidate = ({ _login, loginSuccess }) => {

  const [formCompleted, setFormCompleted] = useState(false);
  const [data, setData] = useState({
    name: '',
    sexo: 'F',
    birthday: '',
    schooling: 'Fundamental - Incompleto',
    nationality: 'Brasileiro',
    mother_name: '',
    father_name: '',
    telephone: '',
    cell_phone: '',
    user_id: '',
    address: null,
    formErrors: {
      name: '',
      birthday: '',
      nationality: '',
      mother_name: '',
      cell_phone: '',
    }
  })

  const history = useHistory();

  let user_id = infoUser('id');
  let document_id = infoUser('document_id');
  let candidate_id = null;

  console.log("#1 Carrega dados do redux: user_id: " + user_id + "document_id" + document_id);

  const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
      val.length > 0 && (valid = false);
    });

    // validate the form was filled out (null vazio)
    Object.values(rest).forEach(val => {
      val === null && (valid = false)
    });

    return valid;
  }


  function infoUser(data) {
    let data_redux;

    try {
      data_redux = _login[0][data];
    } catch (error) {
      console.log(">>> ERRO REDUX, não passou ou user_id");
      history.push('/');
    }

    return data_redux;
  }

  async function handleSubmit(e) {
    console.log("#3 chamou handleSubmit");
    e.preventDefault();

    if (formValid(data)) {

      const userData = {
        user_id: user_id,
        name: data.name,
        sexo: data.sexo,
        birthday: validData(data.birthday),
        schooling: data.schooling,
        nationality: data.nationality,
        mother_name: data.mother_name,
        father_name: data.father_name,
        telephone: data.telephone,
        cell_phone: data.cell_phone
      }

      const addressData = {
        cep: data.address.cep,
        logradouro: data.address.logradouro,
        number: Number(data.address.number),
        complement: data.address.complement,
        district: data.address.bairro,
        city: data.address.localidade,
        uf: data.address.uf,
        user_id: user_id,
      }

      console.log("#4 carregou dados do address", addressData);

      saveCandidates(userData, addressData).then(v => {
            console.log("# loginSucess")
            loginSuccess(user_id);
            localStorage.removeItem("user_id");
            localStorage.setItem("user_id", user_id);   
            history.push('/project');
        });

    } else {
      console.error('FORM INVALID - DISPLAY ERROR MESSAGE')
    }
  };

  async function saveCandidates(userData, addressData) {
    console.log("#5 chamou SaveCandidates")
    const response = await api.post('candidates', userData).then((response) => {

      console.log(`
         --SALVOU-- Candidato
         ${JSON.stringify(response.data)}
       `)

      candidate_id = response.data.id;

    }, (error) => {
      console.log(">>> erro candidates", error);

    }).then(response => {
      console.log("#6 pegou candidate_id", candidate_id)
      saveDataApi({ cpf: document_id, candidate_id: candidate_id }, "documents");

    }).then(response => {
      console.log("#7 pegou addressdData", addressData)
      saveDataApi(addressData, "address")
    });

    console.log(response)
  }


  async function saveDataApi(userData, endpoint) {

    const response = await api.post(endpoint, userData).then((response) => {
      console.log(`
         --SALVOU--
         ${JSON.stringify(response.data)}
       `)

      return response;
    }, (error) => {
      console.log(">>> erro ", endpoint, error);
    });

  }


  function handleChange(e) {
    e.preventDefault();
    let { name, value } = e.target;
    let formErrors = Object.assign(data.formErrors);

    switch (name) {
      case 'name':
        formErrors.name = value.length >= 3
          ? ''
          : 'Favor preencher o nome';
        break;

      case 'nationality':
        formErrors.nationality = value.length >= 3
          ? ''
          : 'Nacionalidade';
        break;

      case 'mother_name':
        formErrors.mother_name = value.length >= 3
          ? ''
          : 'Favor preencher o nome da mãe';
        break;

      case 'cell_phone':
        formErrors.cell_phone = value !== ""
          ? ''
          : 'Favor preencher um número de celular';
        break;

      case 'birthday':
        formErrors.birthday = verifyData(value) ? '' : 'Data inválida'
        break;

      default:
        break;
    }

    const userData = { ...data }

    if (name === 'sexo') {
      value = (value === 'Feminino') ? 'F' : 'M';
    }

    userData[name] = value;
    setData({ formErrors, ...userData });
  }


  // return false se tiver erro e a data se tiver correto
  function verifyData(strData) {
    let isCorrect = true;
    let arrData = strData.split('/'); //MM/DD/YYYY
    let dia = Number(arrData[0]);
    let mes = Number(arrData[1]);
    let ano = Number(arrData[2]);

    // Melhorar
    if (isNaN(dia) || isNaN(mes) || isNaN(ano)) isCorrect = false;
    if (dia === 0 || dia > 31) isCorrect = false;
    if (mes === 0 || mes > 12) isCorrect = false;
    if (ano === 0 || ano <= 1930 || ano >= 2008) isCorrect = false; //Limitado com 12 anos


    if (isCorrect) {
      return new Date(ano, mes - 1, dia);
    }

    return isCorrect;
  }

  function validData(strData) {
    let arrData = strData.split('/'); //MM/DD/YYYY
    let dia = Number(arrData[0]);
    let mes = Number(arrData[1]);
    let ano = Number(arrData[2]);

    return new Date(ano, mes - 1, dia);
  }

  function callbackDataAddress(address_data) {
    console.log("#2 Endereço completo: OK", address_data)
    data.address = address_data;
    setFormCompleted(true);
  }

  return (
    <React.Fragment>
      <GlobalStyle />
      <ContainerPrincipal>
        <Container fluid className="App" >
          <Form className="form col-6" onSubmit={(e) => handleSubmit(e)} noValidate>
            <Row>
              {/* linha pincipal */}
              <Col>
                {/* <span>User_id {user_id}</span> */}
                <img className="logo" src={Logo} alt="" />
                <center><span><strong>Cadastro de usuário</strong></span></center>
                <br />
              </Col>
            </Row>
            <Row>
              <Col className="col-9">
                <Form.Group>
                  <Form.Label>Nome</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    placeholder="Nome"
                    onChange={(e) => handleChange(e)}
                  />
                  {data.formErrors.name.length > 0 && (
                    <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.name}</span>
                  )}
                </Form.Group>
              </Col>

              <Col>
                <Form.Group>
                  <Form.Label>Nacimento</Form.Label>
                  <InputMask
                    name="birthday"
                    mask="99/99/9999"
                    onChange={(e) => handleChange(e)}
                  />
                  {data.formErrors.birthday.length > 0 && (
                    <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.birthday}</span>
                  )}
                </Form.Group>
              </Col>
            </Row>

            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Sexo</Form.Label>
                  <Form.Control
                    as="select"
                    name="sexo"
                    defaultValue="Feminino"
                    onChange={(e) => handleChange(e)}
                  >
                    <option>Feminino</option>
                    <option>Masculino</option>
                  </Form.Control>
                </Form.Group>

              </Col>

              <Col md={6}>
                <Form.Group>
                  <Form.Label>Escolaridade</Form.Label>
                  <Form.Control
                    as="select"
                    name="schooling"
                    onChange={(e) => handleChange(e)}
                  >
                    <option>Fundamental - Incompleto</option>
                    <option>Fundamental - Completo</option>
                    <option>Médio - Incompleto</option>
                    <option>Médio - Completo</option>
                    <option>Superior - Incompleto</option>
                    <option>Superior - Completo</option>
                  </Form.Control>

                </Form.Group>
              </Col>

              <Col>
                <Form.Group>
                  <Form.Label>Nacionalidade</Form.Label>
                  <Form.Control
                    type="text"
                    name="nationality"
                    value={data.nationality}
                    onChange={(e) => handleChange(e)}
                  />
                  {data.formErrors.nationality.length > 0 && (
                    <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.nationality}</span>
                  )}
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Nome da mãe</Form.Label>
                  <Form.Control
                    type="text"
                    name="mother_name"
                    placeholder="Nome da mãe"
                    onChange={(e) => handleChange(e)}
                  />
                  {data.formErrors.mother_name.length > 0 && (
                    <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.mother_name}</span>
                  )}
                </Form.Group>

              </Col>
              <Col>
                <Form.Group>
                  <Form.Label>Nome do pai</Form.Label>
                  <Form.Control
                    type="text"
                    name="father_name"
                    placeholder="Nome do pai"
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>
              </Col>
            </Row>

            <Row>

              <Col md={6}>
                <Form.Group>
                  <Form.Label>Celular</Form.Label>
                  <InputMask
                    className="InputMask"
                    name="cell_phone"
                    mask="+55\ (099) 99999-9999"
                    maskChar=" "
                    onChange={(e) => handleChange(e)}
                  />

                  {data.formErrors.cell_phone.length > 0 && (
                    <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.cell_phone}</span>
                  )}
                </Form.Group>
              </Col>

              <Col>
                <Form.Group>
                  <Form.Label>Telefone Complementar</Form.Label>
                  <InputMask
                    name="telephone"
                    mask="+55\ (099) 99999-9999"
                    maskChar=" "
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Address data={callbackDataAddress} />
            <br />
            <Button variant="primary" type="submit" block disabled={!formCompleted}>
              Confirmar
            </Button>

          </Form>

        </Container>
      </ContainerPrincipal>
    </React.Fragment >
  );
}

RegisterCandidate.prototype = {
  loginSuccess: PropTypes.func.isRequired,
  _login: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    text: PropTypes.string,
  })).isRequired
}


const mapStateToProps = state => ({
  _login: state.login,
})


const mapDispatchToProps = dispatch => bindActionCreators(LoginActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RegisterCandidate);