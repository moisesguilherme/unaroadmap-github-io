import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import {
  Container,
  Col,
  Row,
  Button,
} from "reactstrap";


import Form from 'react-bootstrap/Form'

import Logo from "./../../../assets/imgs/logo_roadmap_small.svg";
import { ContainerPrincipal, GlobalStyle } from './ComponentStyle';

import ViaCep from 'react-via-cep';
//import api from '../../../data/api';
import { cepMask } from './util/FormMask'

export default function RegisterAddress(props) {

  const MSG_ERRO_CEP = 'Favor preencher o CEP.';
  const MSG_ERRO_IVALID_CEP = 'CEP inválido, favor preencher corretamente.';
  const MSG_ERRO_NUMERO = '* preencher';
  const MSG_ERRO_NUMERO_INVALID = 'Inválido';

  const cb = props.data; //Callback para componente pai

  const [data, setData] = useState({
    cep: '',
    bairro: '',
    complement: '',
    logradouro: '',
    localidade: '', //cidade
    uf: '',
    number: '',
    formErrors: {
      cep: '',
      number: '',
    }
  })

  React.useEffect(() => {
    //console.log("Atualizou!")
    if (verifyFormOk()) sendForm(); //Envia os daos para o compoente pai.
  })

  const history = useHistory();

  const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
      val.length > 0 && (valid = false);
    });

    // validate the form was filled out (null vazio)
    Object.values(rest).forEach(val => {
      val === null && (valid = false)
    });

    return valid;
  }

  function verifyFormOk() {
    let formOk = false;
    if ((data.formErrors.cep === '' &&
      data.cep.length >= 9 &&
      data.formErrors.number === '' &&
      data.number.length > 0 &&
      data.logradouro.length > 0 &&
      data.bairro.length > 0 &&
      data.localidade.length > 0 &&
      data.uf.length > 0
    )
    ) formOk = true;

    return formOk;
  }

  function handleChange(e) {
    e.preventDefault();
    let { name, value } = e.target;
    let formErrors = Object.assign(data.formErrors);

    switch (name) {
      case 'cep':
        value = cepMask(e.target.value);
        formErrors.cep = value.length >= 9
          ? ''
          : MSG_ERRO_CEP
        break;
      case 'number':
        formErrors.number = verifiyNumber(e.target.value);
        break;

      default:
        break;
    }

    const userData = { ...data }
    userData[name] = value;
    setData({ formErrors, ...userData });

  }

  function verifiyNumber(value) {
    let formError = '';
    if(isNaN(value)) formError = MSG_ERRO_NUMERO_INVALID;
    if(value === '') formError = MSG_ERRO_NUMERO   
     
    return formError
  }

  function sendForm() {
    console.log(">> enviado dados pro componente pai!");
    const dataAddress = {
      "cep": data.cep,
      "logradouro": data.logradouro,
      "number": data.number,
      "complement": data.complement,
      "district": data.bairro,
      "uf": data.uf
    }
    
    cb(data);
  }

  
  function handleSuccess(cepData) {
    //console.log(cepData);
    if (cepData) {
      const newData = { ...data, ...cepData };
      setData(newData);
    }

    if (cepData.erro) {
      console.log('ERRO CEP - inválido!')

      setErrorMsg(MSG_ERRO_IVALID_CEP, 'cep')
      resetDataForm();
    }
  }

  function resetDataForm() {
    const userData = { ...data }

    userData.logradouro = "";
    userData.bairro = "";
    userData.localidade = "";
    userData.uf = "";
    setData({ ...userData });
  }

  function setErrorMsg(msg, campo) {
    let formErrors = Object.assign(data.formErrors);
    formErrors[campo] = msg;
    setData({ formErrors, ...data });
    console.log(">>> errormsg", data)
  }

  function btnPesquisarCEP(e, fecth) {
    e.preventDefault();

    if (data.cep.length < 9) {
      setErrorMsg(MSG_ERRO_CEP, 'cep')
    } else {
      //callback
      fecth();
    }

  }


  return (
    <>
      <Row>
        <ViaCep cep={data.cep} onSuccess={e => handleSuccess(e)} lazy>
          {({ loading, fetch }) => {
            if (loading) {
              return <p>&nbsp;&nbsp;&nbsp;Carregando...</p>
            }
            return <>
              <Col className="col-9">
                <Form.Group>
                  <Form.Label>CEP</Form.Label>
                  <input onChange={e => handleChange(e)}
                    name="cep"
                    value={data.cep}
                    type="text" />

                  {data.formErrors.cep.length > 0 && (
                    <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.cep}</span>
                  )}
                </Form.Group>

              </Col>
              <Col>
                <Form.Group>
                  <Form.Label>&nbsp;</Form.Label>
                  <button className="special" onClick={(e) => btnPesquisarCEP(e, fetch)}>Pesquisar</button>
                </Form.Group>

              </Col>
            </>
          }}
        </ViaCep>
      </Row>

      <Row>
        <Col className="col-9">
          <Form.Group >
            <Form.Label>Logradouro</Form.Label>
            <Form.Control
              defaultValue={data.logradouro}
              type="text"
              name="logradouro"
              readOnly
            />
          </Form.Group>
        </Col>

        <Col>
          <Form.Group>
            <Form.Label>Número</Form.Label>
            <Form.Control
              type="text"
              name="number"
              onChange={(e) => handleChange(e)}
            />
            {data.formErrors.number.length > 0 && (
              <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.number}</span>
            )}
          </Form.Group>
        </Col>
      </Row>

      <Row>
        <Col >
          <Form.Group>
            <Form.Label>Complemento</Form.Label>
            <Form.Control
              name="complement"
              defaultValue={data.complement}
              onChange={(e) => handleChange(e)}
            >
            </Form.Control>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Bairro</Form.Label>
            <Form.Control
              name="bairro"
              defaultValue={data.bairro}
              readOnly
            >
            </Form.Control>
          </Form.Group>
        </Col>
      </Row>

      <Row>
        <Col>
          <Form.Group>
            <Form.Label>Cidade</Form.Label>
            <Form.Control
              name="localidade"
              defaultValue={data.localidade}
              readOnly
            >
            </Form.Control>
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>Estado</Form.Label>
            <Form.Control
              name="uf"
              defaultValue={data.uf}
              readOnly
            >
            </Form.Control>
          </Form.Group>
        </Col>
      </Row>
      <Row>
      </Row>
    </>
  );
}
