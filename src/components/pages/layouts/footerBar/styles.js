import styled from "styled-components";

export const Container = styled.div `
    background-color: ${props => props.background};
    color: ${props => props.color}; 
    padding-top: 0px;
    padding-bottom: 0px;
    margin-bottom: 0rem !important;
    overflow-x: hidden;
`;
