import React from "react";
import Logo from './../../../../assets/imgs/logo_roadmap_white.svg';
import { Container } from './styles';

export const FooterBar = (props)=>(
   
    <Container background={props.background} color={props.color}>   
        <footer className="container py-5">
            <a name="rodape" />
            <div className="row">
                <div className="col-md-6" align="center">
                    <img src={Logo} alt="logo_roadmap_white" width="160" height="20"/>
                    <small className="d-block mb-3 text-muted">&copy; {new Date().getFullYear()}  Una Roadmap</small>
                </div>
                <div className="col-md-6">
                    <h5>
                        Contato
                    </h5>
                    <ul className="list-unstyled text-small">
                        <li><a className="text-muted" href="/">contato@unaroadmap.com</a></li>
                        <li><a className="text-muted" href="/">(34)8 8808-8880 </a></li>
                        <li><a className="text-muted" href="/">Uberlândia/MG</a></li>              
                    </ul>
                </div>
            </div>
        </footer>
    </Container>
)