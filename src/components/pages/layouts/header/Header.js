import React, { useState } from 'react';
import logo from './../../../../assets/imgs/teste.svg';
import {
  Container,
  Jumbotron,
  Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './estilo.css';

export default function Header() {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

   return (
      <div>
           <Navbar dark expand="md" className="fixed-top">
               <Container>
                    <NavbarBrand href="/">
                      <img src={logo} width="160" height="20" alt="logo_Una_Roadmap" loading="lazy"/>
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="/#inicio">Início</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/#servicos">Serviços</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/#sobre">Sobre Nós</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/#rodape">Contato</NavLink>
                            </NavItem>
                            <NavItem>
                              <a className="btn btn-outline-danger" href="/login" role="button">Entrar</a>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Container>
           </Navbar>
      </div>
  );
}

















// import React from 'react';
// import logo from './../../../../assets/imgs/logo_roadmap_white.svg';
// import { NavLink } from 'react-router-dom';
// import { HeaderComponent, Logo } from './styles';
// import { Header } from './Header';

// export const Header = (props)=>(
//       <HeaderComponent className="header-container" >
//         <div className="header-top">
//           <Logo src={logo} />
//           <NavLink to="/login" className="signIn-btn">Entrar</NavLink>
//         </div>
//       </HeaderComponent >
//   )
