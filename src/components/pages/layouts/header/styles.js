import styled from "styled-components";

//LOGO
export const Logo = styled.img`
    width: 10rem;
    height: 3.5rem;
    position: absolute;
    top: 25%;
    left: 5%;
    transform: translate(0%, -50%);
`;

// Header Container
export const HeaderComponent = styled.div`

    background-color: #000f22;
    height: 70px;
    padding: 0 1rem;
    
    a {
        text-decoration: none;
        color: #fff;
    }


    .signIn-btn {
        right: 0;
        opacity: 0.8;
        margin: 1.125rem 3% 0;
        padding: 0.4375rem 1.0625rem;
        font-weight: 400;
        line-height: normal;
        border-radius: 0.1875rem;
        font-size: 1rem;
        background: white;
        color: #0b212d;
        position: absolute;
        translate: transform(-50%, -50%);
        cursor: pointer;
        transition: background 0.2s ease-in;
        // &:hover {
        //     background : var(--main-red-hover);
        // }
    }

    .signIn-btn:hover {
        opacity: 1;
    }

    // Header Top
    .header-top{
        position: relative;
        height: 10rem;
        z-index: 1;
    }

    // Header Content
    .header-content {
        width: 65%;
        position: relative;
        margin: 4.5rem auto 0;
        display: flex;
        justify-content: center;
        align-content: center;
        text-align: center;
        flex-direction: column;
        z-index: 1;
    }

    // main offer btn
    .main-offer-btn {
        display: inline-block;
        background: var(--main-red);
        text-transform: uppercase;
        border: none;
        outline: none;
        margin: 0 33%;
        padding: 1.5rem;
        border-radius: 0.1875rem;
        font-size: 2rem;
        text-align: center;
        box-shadow: 0 1px 0 rgba(0,0,0,0.45);
        transition: background 0.2s ease-in;
        cursor: pointer;
        &:hover {
            background: var(--main-red-hover);

        }
    }

    .Icon svg {
        vertical-align: bottom;
        margin-left: 1.5rem;
    }
`;
