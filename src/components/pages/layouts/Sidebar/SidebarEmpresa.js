//Sidebar Empresa (Em desenvovimento)

import React from "react";
import { slide as Menu } from "react-burger-menu";
import { Accordion, Nav, Navbar } from "react-bootstrap";
import {
  FaHome,
  FaUsers,
  FaCode,
  FaUserAlt,
  FaDoorOpen,
  FaFileSignature,
  FaClipboard,
} from "react-icons/fa";
import Logo from "../../../../assets/imgs/logo.svg";

export default (props) => {
  return (
    //itens do sidebar
    <Menu>
      <Navbar>
      <Navbar.Brand id="logo" href="/">
            <img
                alt=""
                src={Logo}
                width="70%"
                height="70%"
                className="d-inline-block align-top"
            /></Navbar.Brand>
        <Nav defaultActiveKey="/home" className="flex-column">
          <Nav.Link href="/home">
            <FaHome></FaHome> Pagina Inicial
          </Nav.Link>
          <Nav.Link eventKey="link-1">
            <FaUsers></FaUsers> Candidatos
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaCode></FaCode> Projetos
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaUserAlt></FaUserAlt> Minha conta
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaClipboard></FaClipboard> Relatorios
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaFileSignature></FaFileSignature> Empregos
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaDoorOpen></FaDoorOpen> Logout
          </Nav.Link>
        </Nav>
      </Navbar>
    </Menu>
  );
};
