import React from "react";
import { slide as Menu } from "react-burger-menu";
import { Accordion, Nav, Navbar } from "react-bootstrap";
import {
  FaHome,
  FaBook,
  FaCode,
  FaUserAlt,
  FaDoorOpen,
  FaFileWord,
  FaSearch,
} from "react-icons/fa";
import Logo from "../../../../assets/imgs/logo.svg";

export default (props) => {
  return (
    //itens do sidebar
    <Menu>
      <Navbar>
      <Navbar.Brand id="logo" href="/">
            <img
                alt=""
                src={Logo}
                width="70%"
                height="70%"
                className="d-inline-block align-top"
            /></Navbar.Brand>
        <Nav defaultActiveKey="/home" className="flex-column">
          <Nav.Link href="/home">
            <FaHome></FaHome> Pagina Inicial
          </Nav.Link>
          <Nav.Link eventKey="link-1">
            <FaBook></FaBook> Trilhas
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaCode></FaCode> Projetos
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaUserAlt></FaUserAlt> Minha conta
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaFileWord></FaFileWord> Curriculo
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaSearch></FaSearch> Vagas
          </Nav.Link>
          <Nav.Link eventKey="link-2">
            <FaDoorOpen></FaDoorOpen> Logout
          </Nav.Link>
        </Nav>
      </Navbar>
    </Menu>
  );
};
