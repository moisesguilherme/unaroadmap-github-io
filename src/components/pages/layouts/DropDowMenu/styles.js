import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`

  body {
      margin: 0;
      font-family: roboto;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }

    code {
      font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
        monospace;
    }

    :root {
      --bg:  #242526;
      --bg-accent: #484a4d;
      --text-color: #dadce1;
      --nav-size: 70px;
      --border: 1px solid #474a4d;
      --border-radius: 8px;
      --speed: 500ms; 
    }

`;

export const MenuDropDown = styled.div`
    ul {
      list-style: none;
      margin: 0;
      padding: 0;
    }

    a {
      color: var(--text-color);
      text-decoration: none;;
    }

    .logo{
      display:flex; 
      float: left;
      margin: 10px 40px;
    }


  /* Top Navigation Bar */
    /* <nav> */
    .dw-navbar {
      height: var(--nav-size);
      background-color: #0b212d;
      padding: 0 1rem;
      border-bottom: var(--border);
    }

    /* <ul> */
    .dw-navbar-nav {
      max-width: 100%;
      height: 100%;
      display: flex;
      justify-content: flex-end;
    }

    /* <li> */
    .dw-nav-item {
      width: calc(var(--nav-size) * 0.8);
      display: flex;
      align-items: center;
      justify-content: center;
    }

    /* Icon Button */
    .dw-icon-button {
      --button-size: calc(var(--nav-size) * 0.5);
      width: var(--button-size);
      height: var(--button-size);
      background-color: #0d4665;
      border-radius: 50%;
      padding: 5px;
      margin: 2px;
      display: flex;
      align-items: center;
      justify-content: center;
      transition: filter 300ms;
    }

    .dw-icon-button:hover {
      filter: brightness(1.2);
    }

    .dw-icon-button svg { 
      fill: var(--text-color);
      width: 20px;
      height: 20px;
    }


    /* Dropdown Menu */

    .dw-dropdown {
      position: absolute;
      top: 58px;
      width: 300px;
      transform: translateX(-45%);
      background-color: #0b212d;
      border: var(--border);
      border-radius: var(--border-radius);
      padding: 1rem;
      overflow: hidden;
      transition: height var(--speed) ease;
    }

    .dw-menu {
      width: 100%;
    }

    .dw-menu-item {
      height: 50px;
      display: flex;
      align-items: center;
      border-radius: var(--border-radius);
      transition: background var(--speed);
      padding: 0.5rem;
    }

    .dw-menu-item .dw-icon-button {
      margin-right: 0.5rem;
    }


    .dw-menu-item .dw-icon-button:hover {
      filter: none;
    }

    .dw-menu-item:hover {
      background-color: #525357;
    }

    .dw-icon-right {
      margin-left: auto;
    }

    /* CSSTransition classes  */
    .dw-menu-primary-enter {
      position: absolute;
      transform: translateX(-110%);
    }
    .dw-menu-primary-enter-active {
      transform: translateX(0%);
      transition: all var(--speed) ease;
    }
    .dw-menu-primary-exit {
      position: absolute;
    }
    .dw-menu-primary-exit-active {
      transform: translateX(-110%);
      transition: all var(--speed) ease;
    }


    .dw-menu-secondary-enter {
      transform: translateX(110%);
    }
    .dw-menu-secondary-enter-active {
      transform: translateX(0%);
      transition: all var(--speed) ease;
    }
    .dw-menu-secondary-exit {

    }
    .dw-menu-secondary-exit-active {
      transform: translateX(110%);
      transition: all var(--speed) ease;
    }

`;
