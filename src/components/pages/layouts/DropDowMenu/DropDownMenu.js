import React, { useState } from 'react';
import logo from './../../../../assets/imgs/teste.svg';
import { useHistory } from 'react-router-dom';
import { logout } from '../../../../services/auth';
import {
  Container,
  Jumbotron,
  Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';
//import '../../project/Carousel/node_modules/bootstrap/dist/css/bootstrap.min.css';
import './estilo.css';



export default function Header() {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const history = useHistory();

  function goTo(link) {
    console.log('vair >>' , link);
    history.push( link );
  }
  
  function logOutAndGoToLogin() {
      logout();
      history.push('/login');
  }

   return (
      <div>
           <Navbar dark expand="md" className="fixed-top">
               <Container>
                    <NavbarBrand href="/">
                      <img src={logo} width="160" height="20" alt="logo_Una_Roadmap" loading="lazy"/>
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href='#`' onClick={() => {goTo('/project')}}>Projetos</NavLink>
                            </NavItem>
                            <NavItem>
                              <a className="btn btn-outline-danger" href='#`' onClick={() => logOutAndGoToLogin()} role="button">Sair</a>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Container>
           </Navbar>
      </div>
  );
}

// import { ReactComponent as BellIcon } from './icons/bell.svg';
// import { ReactComponent as MessengerIcon } from './icons/messenger.svg';
// import { ReactComponent as CaretIcon } from './icons/caret.svg';
// import { ReactComponent as PlusIcon } from './icons/plus.svg';
// import { ReactComponent as CogIcon } from './icons/cog.svg';
// import { ReactComponent as ChevronIcon } from './icons/chevron.svg';
// import { ReactComponent as ArrowIcon } from './icons/arrow.svg';
// import { ReactComponent as BoltIcon } from './icons/bolt.svg';

// import React, { useState, useEffect, useRef } from 'react';
// import { Link, useHistory } from 'react-router-dom';
// import { CSSTransition } from 'react-transition-group';

// import {MenuDropDown, GlobalStyle } from './styles';
// import Logo from '../../../../assets/imgs/logo_roadmap_white.svg'

// import { logout } from '../../../../services/auth';

// function MenuTeste() {
  
//   return (
//     <>
//     <GlobalStyle/>
//     <MenuDropDown>
//     <div>
    
//     <Navbar>
//       {/* <NavItem icon={<PlusIcon />} />
//       <NavItem icon={<BellIcon />} />
//       <NavItem icon={<MessengerIcon />} /> */}

//       <NavItem icon={<CaretIcon />}>
//         <DropdownMenu></DropdownMenu>
//       </NavItem>
//     </Navbar>
//     </div>
//     </MenuDropDown>
//     </>
//   );
// }

// function Navbar(props) {
//   return (
//     <>
//     <img  alt="UnaRoadMap"
//     className="logo"
//     src={Logo}
//     width="20%"
//     height="20%"  
//     />
//     <nav className="dw-navbar">
//       <ul className="dw-navbar-nav">{props.children}</ul>
//     </nav>
//     </>
//   );
// }

// function NavItem(props) {
//   const [open, setOpen] = useState(false);

//   return (
//     <li className="dw-nav-item">
//       <a href="#" className="dw-icon-button" onClick={() => {setOpen(!open)}}>
//         {props.icon}
//       </a>

//       {open && props.children}
//     </li>
//   );
// }

// function DropdownMenu() {
//   const history = useHistory();
//   const [activeMenu, setActiveMenu] = useState('main');
//   const [menuHeight, setMenuHeight] = useState(null);
//   const dropdownRef = useRef(null);

//   useEffect(() => {
//     setMenuHeight(dropdownRef.current?.firstChild.offsetHeight)
//   }, [])

//   function calcHeight(el) {
//     const height = el.offsetHeight;
//     setMenuHeight(height);
//   }

//   function goTo(link){
//     console.log('vair >>' , link);
//     history.push( link );
//   }

//   function logOutAndGoToLogin(){
//       logout();
//       history.push('/login');
//   }

//   function DropdownItem(props) {
//     return (
//       <a href="#" className="dw-menu-item" onClick={() => props.link ? props.link() :  (props.goToMenu && setActiveMenu(props.goToMenu))}>
//         <span className="dw-icon-button">{props.leftIcon}</span>
//         {props.children}
//         <span className="dw-icon-right">{props.rightIcon}</span>
//       </a>
//     );
//   }

//   return (
//     <div className="dw-dropdown" style={{ height: menuHeight + 25 }} ref={dropdownRef}>

//       <CSSTransition
//         in={activeMenu === 'main'}
//         timeout={500}
//         classNames="dw-menu-primary"
//         unmountOnExit
//         onEnter={calcHeight}>
//         <div className="dw-menu">
//           <DropdownItem><a href='#`' onClick={() => {goTo('/project')}} >Principal</a></DropdownItem>
//           <DropdownItem><a href='#`' onClick={() => {goTo('/project/trail')}} >Minhas Trilhas</a></DropdownItem>
//           <DropdownItem goToMenu='sair' link={() => logOutAndGoToLogin()}>Sair</DropdownItem>
//           {/* <DropdownItem
//             leftIcon={<CogIcon />}
//             rightIcon={<ChevronIcon />}
//             goToMenu="settings">
//             Cadastro
//           </DropdownItem>
//           <DropdownItem
//             leftIcon="🦧"
//             rightIcon={<ChevronIcon />}
//             goToMenu="animals">
//             Sair
//           </DropdownItem> */}

//         </div>
//       </CSSTransition>

//       <CSSTransition
//         in={activeMenu === 'settings'}
//         timeout={500}
//         classNames="dw-menu-secondary"
//         unmountOnExit
//         onEnter={calcHeight}>
//         <div className="dw-menu">
//           <DropdownItem goToMenu="main" leftIcon={<ArrowIcon />}>
//             <h2>My Tutorial</h2>
//           </DropdownItem>
//           <DropdownItem leftIcon={<BoltIcon />}>HTML</DropdownItem>
//           <DropdownItem leftIcon={<BoltIcon />}>CSS</DropdownItem>
//           <DropdownItem leftIcon={<BoltIcon />}>JavaScript</DropdownItem>
//           <DropdownItem leftIcon={<BoltIcon />}>Awesome!</DropdownItem>
//         </div>
//       </CSSTransition>

//       <CSSTransition
//         in={activeMenu === 'animals'}
//         timeout={500}
//         classNames="dw-menu-secondary"
//         unmountOnExit
//         onEnter={calcHeight}>
//         <div className="dw-menu">
//           <DropdownItem goToMenu="main" leftIcon={<ArrowIcon />}>
//             <h2>Animals</h2>
//           </DropdownItem>
//           <DropdownItem leftIcon="🦘">Kangaroo</DropdownItem>
//           <DropdownItem leftIcon="🐸">Frog</DropdownItem>
//           <DropdownItem leftIcon="🦋">Horse?</DropdownItem>
//           <DropdownItem leftIcon="🦔">Hedgehog</DropdownItem>
//         </div>
//       </CSSTransition>

//       <CSSTransition
//         in={activeMenu === 'sair'}
//         timeout={500}
//         classNames="dw-menu-secondary"
//         unmountOnExit
//         onEnter={calcHeight}
//        />
//     </div>
//   );
// }

// export default MenuTeste;
