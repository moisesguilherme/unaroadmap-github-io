import React, { useState, useEffect } from "react";

import { Link, useHistory, useLocation } from 'react-router-dom';
import api from '../../../services/api';

import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

const Topic = (props) => {

    const history = useHistory();

    function goToLink(code, video=true){
        
        if(video){
            console.log(">>>>> vai", `loadplayer?video=${code}` )
            history.push(`/loadplayer?video=${code}`);
        }else{
            console.log(">>>>> vai", `${code}` )
            window.location.href=`${code}`;
        } 
    }


    return (
            <VerticalTimelineElement
                className="vertical-timeline-element--work"
                contentStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
                contentArrowStyle={{ borderRight: '7px solid  rgb(33, 150, 243)' }}
                date=""
                iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff', borderColor: 'rgb(25, 97, 154)', boxShadow: '0 0 0 4px rgb(25, 97, 154)' }}
                icon={props.icon}
            >
                <h5 className="vertical-timeline-element-title">{props.name}</h5><br />
                <h6 className="vertical-timeline-element-subtitle">{props.description}</h6>
                <p>
                    
                        <button type="button" className="btn btn-light" onClick={ () => goToLink(props.url, props.video)}>
                            Iniciar
                        </button>
                    
                </p>
            </VerticalTimelineElement>
    )
}

export default Topic



