import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
    background: #a3c2c2;
  }
`;
export const ContainerPrincipal = styled.div `
    .container{
        margin: auto;
        width: 50%;
        padding:30px;
    }
    .jumbotron{
        background-color:white;
        margin: auto;
        width: 50%;
    }
    h1{
        text-align: center;
    }

    h6 {
        font-weight: normal;
    }

    .button{
        margin: auto;
    }
    .colors{
        background-color:#C7E8F3;
    }

    a {
        text-decoration: none;
    }
`;