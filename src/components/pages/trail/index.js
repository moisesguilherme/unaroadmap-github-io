import React, { useState, useEffect, useRef } from "react";
import { FooterBar } from '../layouts/footerBar/FooterBar';
import { ContainerPrincipal, GlobalStyle } from './styles';

import { Link, useHistory, useLocation } from 'react-router-dom';
import api from '../../../services/api';

import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

import DropDowMenu from "../layouts/DropDowMenu/DropDownMenu";

import { FaPencilAlt } from 'react-icons/fa';
import { FaStar } from 'react-icons/fa';
import { FaBook } from 'react-icons/fa';

import Topic from './Topic'

const Trial = () => {
    const [init, setInit] = useState(false);
    const [urlForm, setUrlForm] = useState('');
    const [dataTrails, setDataTrails] = useState(null);
    const [dataTopics, setDataTopics] = useState([]);
    const [itensTopic, setDataItensTopics] = useState(null)

    const [_trail, setTrail] = useState(null);
    const isInitialMount = useRef(true);

    useEffect(() => {
        console.log(">>> Atualizou a página")
        initialize();
    }, [init])

    let query = useQuery();
    const project_id = Number(query.get("project"));

    function useQuery() {
        return new URLSearchParams(useLocation().search);
    }

    let arrTrailsIDs = null;
    
    let url = ""
    async function initialize() {
        //localizar todas as trilhas do projeto
        url = await getUrlProject(project_id);
        
        await getTrails(project_id).then( v => {
             console.log(">>>>> trailIds", arrTrailsIDs);
             mountAllTrails();
         })

         console.log(url);
    }

    async function getUrlProject(project_id){
        let url = "";
        const response = await api.get(`projects`).then((response) => {
            
            response.data.map( project => {
                if(project.id === project_id){
                    url = project.str;
                }
            });
        });
        return url;
    }

    function registerProject(project_id){
        localStorage.removeItem("project_id");
        localStorage.setItem("project_id", project_id);
    }

    async function getTrails(project_id) {
        console.log("#1 pega a trilha do projeto:", project_id);
        
        //Registrar o projeto para recuperar na tela de vídeos
        //TODO: Melhorar essa parte.
        registerProject(project_id);

        const response = await api.get(`projects`).then((response) => {
            console.log(`--PEGOU PROJETOS--`)

            console.log(">>>> url", response.data);
            
            // Pega as trilhas do projeto
            const arrIds = [];
            response.data.map(item => {
                if (item.id === project_id) {
                    console.log(">>> pegou id das trilhas", item.trails)
                    const arrTrails = [...item.trails];

                    ///pega o id das trilhas
                    arrTrails.map(trail => {
                        arrIds.push(trail.id)
                    })
                }
            })
            arrTrailsIDs = arrIds;
        });
    }

    // async function getAllTopics(){
    //     let arrFunc = []
    //     for(let i=0; i<arrTrailsIDs.length; i++ ){
    //            arrFunc.push(getTopic(arrTrailsIDs[i]));
    //     }

    //     await Promise.all(arrFunc);
    // }

    //Retorna um objeto com todos os tópicos de uma trilha e o titulo dela
    async function getTopic(trail_id) {
        let objTrail = {}
        const response = await api.get(`trails`).then((response) => {
            console.log(`--PEGOU TOPICOS--`)
            response.data.map(item => {
                console.log("###### tópicos", item);
                if (item.id === trail_id) {
                    console.log(">>>> tópicos", item);
                    objTrail.title = item.name;
                    objTrail.topics = [...item.topics];
                }
            })
        });

        return objTrail;
    }

    async function mountAllTrails(trail_id){
        
        const arrTrails = [];
        for(let i=0; i<arrTrailsIDs.length; i++ ){
           
            let id = arrTrailsIDs[i];
            let objTrilha = await getTopic(id);
            let arrTopicsElements = createTopicsElements(objTrilha.topics);
            
            let isLastTrail = (i === arrTrailsIDs.length - 1) //Se for a última trilha colocar a estrela
            let trilha = mountOneTrail(objTrilha.title, arrTopicsElements, isLastTrail);
            arrTrails.push(trilha);
        }
        
        setTrail(arrTrails);
    }

    function createTopicsElements(arrTopics) {
        const itensTopic = new Array();
        arrTopics.map( item => {
            let topic = <Topic key={item.id} name={item.name} description={item.description} url={item.url} icon={<FaBook/>} video={true}/>
            itensTopic.push(topic);
        });

        return itensTopic;
    }

    // Funcão monta uma  as trilhas
    function mountOneTrail(title, arrTopics,  final=false){
        return ( <>
                 <h1> {title}  </h1>
                 <VerticalTimeline>
                    {(arrTopics !== null) &&
                      arrTopics.map( topic => {
                          return topic
                    })} 

                    {(final===true) && 
                    <>
                    <Topic name="Parabéns, você concluiu toda a trilha!!!" description="Agora queremos saber um pouquinho sobre o seu aprendizado nesta Trilha." url={url} icon={<FaPencilAlt />} video={false} />
                    <VerticalTimelineElement
                            iconStyle={{ background: 'rgb(16, 204, 82)', color: '#fff', boxShadow: '0 0 0 4px rgb(3, 132, 48)' }}
                            icon={<FaStar />}
                     /></>}
                    </VerticalTimeline>
                </>
            )
    }

    return (
        <React.Fragment>
            <DropDowMenu />
            <GlobalStyle />
            <ContainerPrincipal>
                <br />
                <br />
                <br />
                { (_trail !== null) &&
                _trail.map( i => {
                    return i
                })}
            </ContainerPrincipal>
            <FooterBar background="#000f22" color="white"></FooterBar>
        </React.Fragment>
    )
}

export default Trial;