import React, { useState, useEffect } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';



import api from '../../../../services/api';
import dados from './Data';
import WbnPlayer from "./WbnPlayer.js";

//Redux dependecies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as VideoActions from '../../../../store/actions/_video'


async function pegaDados() {
  
    let objTrail = {}
    let trail_id = 1;
    let json = null;
    let arrTrailsIDs = null;
    let project_id = Number(localStorage.getItem("project_id"));
      
    await api.get(`projects`).then((response) => {
      console.log(`--PEGOU PROJETOS--`)
  
      // Pega as trilhas do projeto
      const arrIds = [];
      response.data.map(item => {
        if (item.id === project_id) {
          console.log(">>> pegou id das trilhas", item.trails)
          const arrTrails = [...item.trails];
          ///pega o id das trilhas
          arrTrails.map(trail => {
            arrIds.push(trail.id)
          })
        }
      })
      arrTrailsIDs = arrIds;
    })
  
    let num = 0;
    let arrData = [];
    for (let i = 0; i < arrTrailsIDs.length; i++) {
  
      let trail_id = arrTrailsIDs[i];
  
      const response = await api.get(`trails/${trail_id}`).then((response) => {
        console.log(`--PEGOU OS TOPICOS-- ${trail_id}`)
  
        response.data.topics.map(topic => {
          console.log(">>> topico", topic);
          let obj = { "num": num++, "title": topic.name, "id": topic.url, duration: "8:00", "video": "https://www.youtube.com/embed/" + topic.url }
          console.log(">>> obj", obj)
          arrData.push(obj);
        })
  
      })
    }
  
    // .then(v => {
  
    //   // var objt = {
    //            "playlistId": "pro_1",
    //            "playlist": array
    //             }
    //   json = {
    //     "playlistId": "wbn_rdx",
    //     "playlist":
    //       [{ "num": 1, "title": "aaaa e CSS  Básico - Tutorial", "id": "HWAsIEYpdWY", "duration": "8:25", "video": "https://www.youtube.com/embed/HWAsIEYpdWY" },
    //       { "num": 2, "title": "GitHub Parte 1", "id": "uOIETZIQvV4", "duration": "13:27", "video": "https://www.youtube.com/embed/8rMd1QO8GnE" },
    //       { "num": 3, "title": "GitHub Parte 2", "id": "GzRxUO41noY", "duration": "14:56", "video": "https://www.youtube.com/embed/Mn-H_69847Y" }
    //       ]
    //   }
    // })
    //json =
    return arrData;
}  

const LoadPlayer = (props) => {
    const history = useHistory();


    if(localStorage.getItem("voltar")){
        localStorage.removeItem("voltar");

        let  project_id = Number(localStorage.getItem("project_id")); 
        //history.push('/project/trail?project=' + project_id);
        history.goBack();
    }
    
    
    const [init, setInit] = useState(null);
    const [loading, setLoading] = useState(null);
    
    
    let dados;

    let query = useQuery();
    let video_url = query.get("video");

    function useQuery() {
        return new URLSearchParams(useLocation().search);
      }
          
  
    useEffect(() => {
      initializing();
    }, [init]);
  
    
    async function initializing() {
      console.log(">>>>>>>>> antes dados")
      dados = await pegaDados();
      

      console.log(">>>>> terminou!!!!", dados)
      //videos = dados;      
      setLoading(true);
      localStorage.removeItem("data_video");
      
      const data = {"playlistId":"wbn_rdx",
      "playlist": dados
      }

      
      localStorage.setItem("data_video", JSON.stringify(data));   
      
      
      history.push('/videoplayer/' + video_url);
      
      //let newState = Object.assign(state);
      //newState.videos = dados//.push({ "num": 2, "title": "xxxxx1", "id": "uOIETZIQvV4", "duration": "13:27", "video": "https://www.youtube.com/embed/8rMd1QO8GnE" });
      ///setState(newState)
    }
  
    return (
        <div>
            <p>Carregando....</p>
            {(loading===true) && <span>Pronto..!</span>}
        </div>
    )
}

export default LoadPlayer;


// RegisterCandidate.prototype = {
//     sendVideo: PropTypes.func.isRequired,
//     _video: PropTypes.arrayOf(PropTypes.shape({
//       dados: PropTypes.object,
//     })).isRequired
//   }
  
  
//   const mapStateToProps = state => ({
//     _video: state.video,
//   })
  
  
//   const mapDispatchToProps = dispatch => bindActionCreators(VideoActions, dispatch);
  
//   export default connect(mapStateToProps, mapDispatchToProps)(LoadPlayer);