import React, { useState } from "react";
//Redux dependecies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as LoginActions from '../../../store/actions/_login'

import { trackPromise, usePromiseTracker } from 'react-promise-tracker';

import { Link, useHistory } from 'react-router-dom';
import {
  Container,
  Col,
  Row,
  Form,
  Input,
  Label,
  FormGroup,
  Button,
} from "reactstrap";
import Logo from "./../../../assets/imgs/logo_roadmap_small.svg";
import { ContainerPrincipal, GlobalStyle } from './styles';

import Loading from './Loading';
import sendEmail from '../../../services/sendEmail';

const Forgot = ({ _login, loginSuccess }) => {

  const MSG_ERROR = 'E-mail não cadastrado no sistema';
  const MSG_INVALID_EMAIL = 'Digite um Email válido.';
  const MSG_SUCESS = "Uma nova senha foi enviada para o seu email.";

  const { promiseInProgress } = usePromiseTracker();

  const [statusError, setStatusError] = useState('');
  const [statusSuccess, setStatusSuccess] = useState('');
  const [email, setEmail] = useState(null);

  const history = useHistory();

  const formValid = (data) => {
    return (email !== null)
  }

  function handleChange(e) {
    e.preventDefault();

    if(e.target.name === "email"){
      setEmail(e.target.value);
    }

  }

  function validateEmail(e) {
    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let status = '';

    if (!emailRex.test(e.target.value)) {
      status = MSG_INVALID_EMAIL;
    }
    setStatusError(status);
  }

  //Chama api       
  async function handleSubmit(e) {
    e.preventDefault();
    
      //Para funcionar o loading
      trackPromise(
        verifyUser()
      )
  }

  //
  async function verifyUser() {
    
    ///Vai chamar a api e pegar os dados
    console.log(">>>>> email:", email);
    const userData = {
      usuario: "Moisés",
      email: email,
      senha: "random123123"
    }

    //    
    const response = await sendEmail.post('send-mail', userData).then((response) => {
      console.log(`
      --SALVOU--
      ${JSON.stringify(response.data)}
    `)
      setStatusSuccess(MSG_SUCESS);

    }, (error) => {
      console.log(">>>", error);
      setStatusError(MSG_ERROR);
    });
  }
  
  return (
    <React.Fragment>
      <GlobalStyle />
      <ContainerPrincipal>
        <Container fluid className="App">
          <Form className="form" onSubmit={(e) => handleSubmit(e)} noValidate>
            <Col>
              <img className="logo" src={Logo} alt="" />
              <FormGroup>
                <Label>
                  <strong>Digite o seu Email</strong>
                </Label>
                <Input
                  type="email"
                  name="email"
                  id="exampleEmail"
                  placeholder="Email"
                  onChange={(e) => { handleChange(e); validateEmail(e) }}
                  noValidate
                />
                {statusError.length > 0 && (
                  <span className="errorMessage" style={{ color: 'red' }}>{statusError}</span>
                )}
                {statusSuccess.length > 0 && (
                  <span className="errorMessage" style={{ color: 'green' }}>{statusSuccess}</span>
                )}

              </FormGroup>
            </Col>
            <Button>Enviar</Button>
            <div>
            {
              (promiseInProgress === true) ? <Loading /> : null
            }
            </div>

            <Row className="linkbar">
              <Col className="justify-left">
                <Link className="linkBtn" to="/login">Login</Link>
              </Col>
              <Col className="justify-right">
                <Link className="linkBtn" to="/register/user">Cadastrar</Link>
              </Col>
            </Row>
          </Form>
        </Container>
      </ContainerPrincipal>
    </React.Fragment >
  );
}

Forgot.prototype = {
  loginSuccess: PropTypes.func.isRequired,
  _login: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    text: PropTypes.string,
  })).isRequired
}

const mapStateToProps = state => ({
  _login: state.login,
})

const mapDispatchToProps = dispatch => bindActionCreators(LoginActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Forgot);
