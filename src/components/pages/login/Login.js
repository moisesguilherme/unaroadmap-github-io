import React, { useState } from "react";
import { Link, useHistory } from 'react-router-dom';

//Api e Atutenticação
import api from '../../../services/api';
import { login } from '../../../services/auth';

import { trackPromise, usePromiseTracker } from 'react-promise-tracker';
import Loading from './Loading';

//Redux dependecies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as LoginActions from '../../../store/actions/_login'

import 'bootstrap/dist/css/bootstrap.min.css';
import './estilos.css';

//Reactstrap
import {
  Container,
  Col,
  Row,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import { ContainerPrincipal, GlobalStyle } from './stylesLogin';
import Logo from "./../../../assets/imgs/logo_roadmap_small.svg";
import imgLogo from "./../../../assets/imgs/logo_roadmap_small_black.svg";


const Login = ({ _login, loginSuccess }) => {

  const { promiseInProgress } = usePromiseTracker();

  const MSG_INVALID = 'Senha ou E-mail inválido';
  const MSG_ERRO = 'Email inválido.'
  const MSG_DELETED = 'Usuário desativado procure o adminstrador.'

  const [data, setData] = useState({
    email: '',
    password: '',
    error: ''
  })

  const [statusError, setStatusError] = useState('')
  const history = useHistory();

  const formValid = (data) => {
    let valid = true;

    if (data.email == null || data.password == null) {
      valid = false;
    }
    return valid;
  }

  function handleChange(e) {
    e.preventDefault();
    const { name, value } = e.target;
    const userData = { ...data }
    userData[name] = value;
    setData({ ...userData });

    //TODO: Arrumar essa parte
    if (name === 'password' && statusError === MSG_INVALID) {
      setStatusError('');
    }
  }

  function validateEmail(e) {
    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let status = '';

    if (!emailRex.test(e.target.value)) {
      status = MSG_ERRO
    }
    setStatusError(status);
  }


  function verifyFormOk() {
        let formOk = false;
    
        if (statusError.length === 0 &&
          data.password.length > 0 &&
          data.email.length > 0)
          formOk = true;
    
        return formOk
      }


  async function handleSubmit(e) {
    e.preventDefault();

    if (formValid(data)) {

      //Para funcionar o loading
      trackPromise(
        validateLogin()
      )

    } else {
      console.error('Erro o campo email ou senha não foram preenchidos.')
    }

  }

  async function validateLogin() {
      const {email, password } = data;
      let redirectPage = "/project"

      try {

        const response = await api.post("/users/login", { email, password });
        if(response.data.status === "Inactive" || response.data.status === "Deleted" ){
           setStatusError(MSG_DELETED);
        }else{
          
          // Redireciona para admin ou admin para empresas
          if(response.data.profile === 'Administrador') redirectPage = '/admin';
          if(response.data.profile === 'Empresa') redirectPage = '/admin/company';          
          loginSuccess(response.data.id); //Redux
          login(response.data.token); //Autenticacao
          localStorage.removeItem("user_id");
          localStorage.setItem("user_id", response.data.id);   
          history.push(redirectPage);
        }
               

      } catch (err) {
        console.log("Houve um problema com o login, verifique suas credenciais." + err);
        setStatusError(MSG_INVALID);
        
      }
  }

return (
  <React.Fragment>
      <GlobalStyle />
      <section className="container-fluid">
        <div className="row justify-content-center">
          <div className="col-md-4"></div>
          <div className="col-md-4">
            <form className="form-container" onSubmit={(e) => handleSubmit(e)} noValidate>
              <div className="d-flex justify-content-center">
                <img src={imgLogo} alt="logo_Una_Roadmap" loading="lazy"/>
              </div>
              <div className="mt-4">
                <a href="/register/user" className="float-right btn btn-outline-primary">Cadastrar</a>
                <h4 className="card-title mb-4 mt-1">Login</h4>
                <div className="form-group">
                  <input 
                    name="email"
                    id="exampleEmail"
                    type="email" 
                    className="form-control" 
                    aria-describedby="emailHelp" 
                    placeholder="E-mail..." 
                    onChange={(e) => { handleChange(e); validateEmail(e) }}
                    noValidate
                  />
                </div>
                <div className="form-group">
                  <Input
                    type="password"
                    name="password"
                    placeholder="Senha.."
                    onChange={(e) => handleChange(e)}
                  />
                </div>
                {statusError.length > 0 && (
                  <small className="form-text text-muted" id="teste">{statusError}</small>
                )}  
                {
                  console.log(!verifyFormOk())
                }
                <Button type="submit" block disabled={!verifyFormOk()} className="btn btn-danger botoes">Entrar</Button>
                {/* <button type="submit" className="btn btn-danger btn-block botoes disabled mt-2">ENTRAR</button> */}
                <div>
                  {
                    (promiseInProgress === true) ? <Loading /> : null
                  }
                </div>
                <p className="text-center">
                    <a href="/login/forgot" className="btn">Esqueceu a senha ?</a>
                </p>
              </div>
            </form>
          </div>
          <div className="col-md-4"></div>
        </div>
      </section>
  </React.Fragment >
);
}

Login.prototype = {
  loginSuccess: PropTypes.func.isRequired,
  _login: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    text: PropTypes.string,
  })).isRequired
}

const mapStateToProps = state => ({
  _login: state.login,
})

const mapDispatchToProps = dispatch => bindActionCreators(LoginActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);


// import React, { useState } from "react";
// import { Link, useHistory } from 'react-router-dom';

// //Api e Atutenticação
// import api from '../../../services/api';
// import { login } from '../../../services/auth';

// import { trackPromise, usePromiseTracker } from 'react-promise-tracker';
// import Loading from './Loading';

// //Redux dependecies
// import PropTypes from 'prop-types';
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux'
// import * as LoginActions from '../../../store/actions/_login'

// //Reactstrap
// import {
//   Container,
//   Col,
//   Row,
//   Form,
//   FormGroup,
//   Label,
//   Input,
//   Button,
// } from "reactstrap";
// import { ContainerPrincipal, GlobalStyle } from './styles';
// import Logo from "./../../../assets/imgs/logo_roadmap_small.svg";


// const Login = ({ _login, loginSuccess }) => {

//   const { promiseInProgress } = usePromiseTracker();

//   const MSG_INVALID = 'Senha ou E-mail inválido';
//   const MSG_ERRO = 'Email inválido.'
//   const MSG_DELETED = 'Usuário desativado procure o adminstrador.'

//   const [data, setData] = useState({
//     email: '',
//     password: '',
//     error: ''
//   })

//   const [statusError, setStatusError] = useState('')
//   const history = useHistory();

//   const formValid = (data) => {
//     let valid = true;

//     if (data.email == null || data.password == null) {
//       valid = false;
//     }
//     return valid;
//   }

//   function handleChange(e) {
//     e.preventDefault();
//     const { name, value } = e.target;
//     const userData = { ...data }
//     userData[name] = value;
//     setData({ ...userData });

//     //TODO: Arrumar essa parte
//     if (name === 'password' && statusError === MSG_INVALID) {
//       setStatusError('');
//     }
//   }

//   function validateEmail(e) {
//     const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     let status = '';

//     if (!emailRex.test(e.target.value)) {
//       status = MSG_ERRO
//     }
//     setStatusError(status);
//   }


//   function verifyFormOk() {
//     let formOk = false;

//     if (statusError.length === 0 &&
//       data.password.length > 0 &&
//       data.email.length > 0)
//       formOk = true;

//     return formOk
//   }


//   async function handleSubmit(e) {
//     e.preventDefault();

//     if (formValid(data)) {

//       //Para funcionar o loading
//       trackPromise(
//         validateLogin()
//       )

//     } else {
//       console.error('Erro o campo email ou senha não foram preenchidos.')
//     }

//   }

//   async function validateLogin() {
//       const {email, password } = data;
//       let redirectPage = "/project"

//       try {

//         const response = await api.post("/users/login", { email, password });
//         if(response.data.status === "Inactive" || response.data.status === "Deleted" ){
//            setStatusError(MSG_DELETED);
//         }else{
          
//           // Redireciona para admin ou admin para empresas
//           if(response.data.profile === 'Administrador') redirectPage = '/admin';
//           if(response.data.profile === 'Empresa') redirectPage = '/admin/company';          
//           loginSuccess(response.data.id); //Redux
//           login(response.data.token); //Autenticacao
//           localStorage.removeItem("user_id");
//           localStorage.setItem("user_id", response.data.id);   
//           history.push(redirectPage);
//         }
               

//       } catch (err) {
//         console.log("Houve um problema com o login, verifique suas credenciais." + err);
//         setStatusError(MSG_INVALID);
        
//       }
//   }

// return (
//   <React.Fragment>
//     <GlobalStyle />
//     <ContainerPrincipal>
//       <Container fluid className="App">
//         <Form className="form" onSubmit={(e) => handleSubmit(e)} noValidate>
//           <Col>
//             <img className="logo" src={Logo} alt="" />
//             <FormGroup>
//               <Label>
//                 <strong>Email</strong>
//               </Label>
//               <Input
//                 type="email"
//                 name="email"
//                 id="exampleEmail"
//                 placeholder="Email"
//                 onChange={(e) => { handleChange(e); validateEmail(e) }}
//                 noValidate
//               />

//             </FormGroup>
//           </Col>
//           <Col>
//             <FormGroup>
//               <Label for="examplePassword">
//                 <strong>Senha</strong>
//               </Label>
//               <Input
//                 type="password"
//                 name="password"
//                 placeholder="Password"
//                 onChange={(e) => handleChange(e)}
//               />
//               {statusError.length > 0 && (
//                 <span className="errorMessage" style={{ color: 'red' }}>{statusError}</span>
//               )}
//             </FormGroup>
//           </Col>
//           <Button type="submit" block disabled={!verifyFormOk()}>
//             Login
//             </Button>
//           <div>
//             {
//               (promiseInProgress === true) ? <Loading /> : null
//             }
//           </div>
//           <Row className="linkbar">
//             <Col className="justify-left">
//               <Link className="linkBtn" to="/login/forgot">Esqueci a Senha</Link>
//             </Col>
//             <Col className="justify-right">
//               <Link className="linkBtn" to="/register/user">Cadastrar</Link>
//             </Col>
//           </Row>
//         </Form>
//       </Container>
//     </ContainerPrincipal>
//   </React.Fragment >
// );
// }

// Login.prototype = {
//   loginSuccess: PropTypes.func.isRequired,
//   _login: PropTypes.arrayOf(PropTypes.shape({
//     id: PropTypes.number,
//     text: PropTypes.string,
//   })).isRequired
// }

// const mapStateToProps = state => ({
//   _login: state.login,
// })

// const mapDispatchToProps = dispatch => bindActionCreators(LoginActions, dispatch);

// export default connect(mapStateToProps, mapDispatchToProps)(Login);

