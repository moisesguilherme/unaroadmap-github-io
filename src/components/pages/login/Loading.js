import React from 'react'
import { css  } from '@emotion/core';
import ClipLoader from 'react-spinners/PropagateLoader';
import styled from 'styled-components';

const override = css`
  display:flex;
  margin-top: 10px;
  margin-bottom: 10px;
  justify-content: center;
`;


export default function Loading() {
    return (
            <ClipLoader
                css={override}
                size={15}
                color={'#e31937'}
                loading='true'
            />
    )
}
