import React, { useState } from "react";


import { trackPromise, usePromiseTracker } from 'react-promise-tracker';
import Loading from './Loading';

import { Link, useHistory, useLocation } from 'react-router-dom';
import {
  Container,
  Col,
  Row,
  Form,
  Input,
  Label,
  FormGroup,
  Button,
} from "reactstrap";
import Logo from "./../../../assets/imgs/logo_roadmap_small.svg";
import { ContainerPrincipal, GlobalStyle } from './styles';

//Api e Atutenticação
import api from '../../../services/api';
import { login } from '../../../services/auth';

//Redux dependecies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as LoginActions from '../../../store/actions/_login'

const NewPassword = ({ _login, loginSuccess }) => {

  const { promiseInProgress } = usePromiseTracker();
  const MSG_INVALID = 'Senha inválida';
  const MSG_DELETED = 'Usuário desativado procure o adminstrador.'

  let tempUserId = null;

  const history = useHistory();
  let query = useQuery();

  const [statusError, setStatusError] = useState('')
  const [data, setData] = useState({
    password: '',
    newPassword: '',
    repeatPassword: '',
    formErrors: {
      password: '',
      newPassword: '',
    }
  })


  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }

  function verifyFormOk() {
    
    return ((data.formErrors.password === '' && data.password.length > 0) &&
        (data.formErrors.newPassword === '' && data.newPassword.length > 0) &&
        data.newPassword === data.repeatPassword
       ) 
  }


  const formValid = (data) => {
    let valid = true;

    if (data.email == null || data.password == null) {
      valid = false;
    }
    return valid;
  }

  function handleChange(e) {
    e.preventDefault();

    const { name, value } = e.target;
    let formErrors = Object.assign(data.formErrors);
    const userData = { ...data }

    switch (name) {
      case 'password':
        formErrors.password = value.length < 6 && value.length > 0
          ? 'Mínimo de 6 letras'
          : '';
        break;

        case 'newPassword':
        formErrors.newPassword = value.length < 6 && value.length > 0
          ? 'Mínimo de 6 letras'
          : '';
        break;

      default:
        break;
    }

    userData[name] = value;
    setData({ formErrors, ...userData });
    setStatusError('');
  }


  //Chama api       
  async function handleSubmit(e) {
    e.preventDefault();

    console.log(">>> chamou handleSubmit")

    trackPromise(
      verifyPassword()
    )
  }

  async function validateLogin() {
    //verifyPassword()
    //console.log(">>> e o mesmo id:" , tempUserId, query.get("id"))
    // console.log(">>> mudar passsword"
  }

  // function isTheSameUser(){
  //   return tempUserId === query.get("id")
  // }

  //verificar se a senha bate
  // se sim gravar a senha nova um update
  // logar no sistema
  async function verifyPassword() {
    const password = data.password;
    const email = query.get("email");
    let redirectPage = "/project"

    try {

      const response = await api.post("/users/login", { email, password });
      const user_id = response.data.id;
      console.log(">>> pegou usuário", response.data);
      

      if (response.data.status === "Inactive" || response.data.status === "Deleted") {
        setStatusError(MSG_DELETED);
      } else {

        login(response.data.token); //Autenticacao
        changePassword(user_id);

        // Redireciona para admin ou admin para empresas
        if(response.data.profile === 'Administrador') redirectPage = '/admin';
        if(response.data.profile === 'Empresa') redirectPage = '/admin/company';          
        loginSuccess(response.data.id); //Redux
        history.push(redirectPage);
      }

    } catch (err) {
      console.log("Houve um problema com o login, verifique suas credenciais." + err);
      setStatusError(MSG_INVALID);
    }
  }

  async function changePassword(user_id) {
    const userData = { password: data.newPassword };
    console.log(">>> changePassword", userData, user_id)
    const response = await api.put(`users/${user_id}`, userData ).then((response) => {

    console.log(">>>> mudou o password", response);

    }, (error) => {
      console.log(">>>", error);
    })
  }

  return (
    <React.Fragment>
      <GlobalStyle />
      <ContainerPrincipal>
        <Container fluid className="App">
          <Form className="form" onSubmit={(e) => handleSubmit(e)} noValidate>
            <Col>
              <img className="logo" src={Logo} alt="" />
              <FormGroup>
                <Label>
                  <strong>Senha Atual</strong>
                </Label>
                <Input
                  type="password"
                  name="password"
                  id="password"
                  placeholder="Senha atual"
                  onChange={(e) => { handleChange(e) }}
                  noValidate
                />
                {data.formErrors.password.length > 0 && (
                  <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.password}</span>
                )}
              </FormGroup>
            </Col>

            <Col>
              <FormGroup>
                <Label>
                  <strong>Nova Senha</strong>
                </Label>
                <Input
                  type="password"
                  name="newPassword"
                  id="newPassword"
                  placeholder="Nova senha"
                  onChange={(e) => { handleChange(e) }}
                  noValidate
                />
                {data.formErrors.newPassword.length > 0 && (
                  <span className="errorMessage" style={{ color: 'red' }}>{data.formErrors.newPassword}</span>
                )}
              </FormGroup>
            </Col>

            <Col>
              <FormGroup>
                <Label>
                  <strong>Confime a Senha</strong>
                </Label>
                <Input
                  type="password"
                  name="confirm"
                  name="repeatPassword"
                  placeholder="Confirme a senha"
                  onChange={(e) => { handleChange(e) }}
                  noValidate
                />
              </FormGroup>
            </Col>
            <Button block disabled={!verifyFormOk()}>Enviar</Button>
            <br/>
            <div>
              {
                (promiseInProgress === true) ? <Loading /> : null
              }
            </div>
            <div>
              {statusError.length > 0 && (
                  <span className="errorMessage" style={{ color: 'red' }}>{statusError}</span>
              )}
            </div>
            <Row className="linkbar">
              <Col className="justify-left">
                <Link className="linkBtn" to="/login">Login</Link>
              </Col>
              <Col className="justify-right">
                <Link className="linkBtn" to="/register/user">Cadastrar</Link>
              </Col>
            </Row>
          </Form>
        </Container>
      </ContainerPrincipal>
    </React.Fragment >
  );
}

NewPassword.prototype = {
  loginSuccess: PropTypes.func.isRequired,
  _login: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    text: PropTypes.string,
  })).isRequired
}

const mapStateToProps = state => ({
  _login: state.login,
})

const mapDispatchToProps = dispatch => bindActionCreators(LoginActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NewPassword);
