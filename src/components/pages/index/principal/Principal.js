import React from 'react';
import {
  Container,
  Jumbotron,
  Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './estilo.css';

export const Principal = () => (
  <Jumbotron fluid className="descr-top">
    <Container className="text-center">
        <a name="inicio" className="inicio-link" />
        <p className="lead mb-4 dev">Qualifique-se e construa sua carreira</p>
        <p className="lead mb-4">Você + Projetos Reais + Tecnologias de Desenvolvimento</p>
        <p className="lead">
            <a className="btn btn-danger mr-2 botoes" href="/#servicos" role="button">SAIBA MAIS</a>
            <a className="btn btn-info botoes" href="/register/user" role="button">REGISTRAR</a>   
        </p>
    </Container>      
  </Jumbotron>
);

// import React from 'react';

// import { NavLink, Link } from 'react-router-dom'
// import { ContainerPrincipal, Title, SubTitle } from './styles';
// import { ic_keyboard_arrow_right } from 'react-icons-kit/md/ic_keyboard_arrow_right'
// import bgImage from "../../../assets/imgs/teste3.jpg";
// import { Icon } from 'react-icons-kit';

// export const Principal = () => (
//   <ContainerPrincipal className="header-container" img={bgImage}>
//     {/* Header Content */}
//     <div className="header-content" >
//       <br /><br />
//       <Title>Qualifique-se e construa sua carreira</Title>
//       <SubTitle>Você + Projetos Reais + Tecnologias de Desenvolvimento</SubTitle>
//       <div className="buttons">
//         <a href="#services" className="btn red block">SABER MAIS</a>
//         <Link to="/register/user" className="btn sky block">
//           REGISTRAR
//             {/* <Icon className="Icon" icon={ic_keyboard_arrow_right} size={37} /> */}
//         </Link>
//       </div>  
//     </div>
//   </ContainerPrincipal>
// );