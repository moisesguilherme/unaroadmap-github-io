import React from 'react';
import {
  Container,
  Jumbotron
} from 'reactstrap';
import { FaProjectDiagram, FaTrophy, FaMapSigns} from 'react-icons/fa';
import 'bootstrap/dist/css/bootstrap.min.css';
import './estilo.css';

export const Servicos = () => (
  <Jumbotron fluid className="servicos">
    <Container className="text-center">
        <a name="servicos" className="servicos-link" />
        <div>
            <h2 className="display-4">Serviços</h2>
            <p className="lead pb-4">No que podemos te ajudar?</p>
        </div>
        <div className="row">
            <div className="col-md-4">
                <div className="rounded-circle circulo centralizar">
                    <FaProjectDiagram />
                </div>
                <h2 className="mt-4 mb-4">Projetos Reais</h2>
                <p>Trazemos através de projetos reais, conteúdos disponibilizados em <strong>Trilhas de Aprendizagem</strong>, com objetivo direto ao mercado de trabalho. Acesso totalmente gratuito em níveis iniciante, 
                intermediário e avançado.</p>
            </div>
            <div className="col-md-4">
                <div className="rounded-circle circulo centralizar">
                    <FaTrophy/>
                </div>
                <h2 className="mt-4 mb-4" >Melhoria Profissional</h2>
                <p>Seja um profissional de tecnologia, estamos aqui para ajudar VOCÊ nesta conquista, para seu Futuro Profissional. 
                VOCÊ pode construir aqui seu portfólio, e utilizar gratuitamente, ao final de cada oportunidade, 
                você já é direcionado para o processo seletivo, do projeto.</p>
            </div>
            <div className="col-md-4">
                <div className="rounded-circle circulo centralizar">
                    <FaMapSigns />
                </div>
                <h2 className="mt-4 mb-4" >Várias Áreas</h2>
                <p className="menor"><strong>Front-end:</strong> Design e Criação de interfaces gráficas em uma aplicação.</p>
                <p className="menor"><strong>Back-end:</strong> Implementação da lógica de negócio, interações com bancos de dados e performance de uma aplicação.</p>
                <p className="menor"><strong>Banco de Dados:</strong> Responsável por gerenciar, instalar, configurar, atualizar e monitorar um ou vários bancos de dados</p>
            </div>
        </div>          
    </Container>
  </Jumbotron>
);

// import React from 'react';

// import { NavLink, Link } from 'react-router-dom'
// import { ContainerPrincipal, Title, SubTitle } from './styles';
// import { ic_keyboard_arrow_right } from 'react-icons-kit/md/ic_keyboard_arrow_right'
// import bgImage from "../../../assets/imgs/teste3.jpg";
// import { Icon } from 'react-icons-kit';

// export const Principal = () => (
//   <ContainerPrincipal className="header-container" img={bgImage}>
//     {/* Header Content */}
//     <div className="header-content" >
//       <br /><br />
//       <Title>Qualifique-se e construa sua carreira</Title>
//       <SubTitle>Você + Projetos Reais + Tecnologias de Desenvolvimento</SubTitle>
//       <div className="buttons">
//         <a href="#services" className="btn red block">SABER MAIS</a>
//         <Link to="/register/user" className="btn sky block">
//           REGISTRAR
//             {/* <Icon className="Icon" icon={ic_keyboard_arrow_right} size={37} /> */}
//         </Link>
//       </div>  
//     </div>
//   </ContainerPrincipal>
// );