import styled from "styled-components";


export const ContainerPrincipal = styled.div`
    
    background: radial-gradient(rgba(0,0,0,0), rgba(0,0,0,0.7)),
    url(${props => props.img}) no-repeat center center/cover;
    color: #fff;
    height: 92vh;
    display: flex;
    justify-content: center;
    align-content: center;
    text-align: center;
  
    a {
        text-decoration: none;
        color: #fff;
    }
    /* Header Section */
   .header-container {
        position: relative;    
        width: 100vw;
    }

    .header-container::after {
        content: '';
        position: absolute;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.5);
        box-shadow: inset 4.374rem 3.125rem 11.875rem #000,
        inset -4.375rem -3.125rem -11.875rem;
    }

    // Header Content
    .header-content {
        width: 65%;
        position: relative;
        margin: 0rem auto 0;
        display: flex;
        justify-content: center;
        align-content: center;
        text-align: center;
        flex-direction: column;
        z-index: 1;
    }

    
    .btn {
        padding: 1em 2.1em 1.1em;
        border-radius: 3px;
        margin: 8px 8px 8px 8px;
        color: #fbdedb;
        background-color: #fbdedb;
        display: inline;
        background: #e74c3c;
        -webkit-transition: 0.3s;
        -moz-transition: 0.3s;
        -o-transition: 0.3s;
        transition: 0.3s;
        font-family: sans-serif;
        font-weight: 800;
        font-size: .85em;
        text-transform: uppercase;
        text-align: center;
        text-decoration: none;
        -webkit-box-shadow: 0em -0.3rem 0em rgba(0, 0, 0, 0.1) inset;
        -moz-box-shadow: 0em -0.3rem 0em rgba(0, 0, 0, 0.1) inset;
        box-shadow: 0em -0.3rem 0em rgba(0, 0, 0, 0.1) inset;
        position: relative;
    }
    .btn:hover, .btn:focus {
        opacity: 0.8;
    }
    .btn:active {
        -webkit-transform: scale(0.80);
        -moz-transform: scale(0.80);
        -ms-transform: scale(0.80);
        -o-transform: scale(0.80);
        transform: scale(0.80);
    }
    // .btn.block {
    //     display: block !important;
    // }

    .sky {
        background-color: #6698cb;
    }

    .red {
        background-color: #e8304c;
    }

    // main offer btn
    .main-offer-btn {
        // opacity: 0.8;
        // display: inline-block;
        // background: #0b212d;
        // text-transform: uppercase;
        // border: none;
        // outline: none;
        // margin: 0 33%;
        // padding: 1.5rem;
        // border-radius: 0.1875rem;
        // font-size: 1.5rem;
        // text-align: center;
        // box-shadow: 0 1px 0 rgba(0,0,0,0.45);
        // transition: background 0.2s ease-in;
        // cursor: pointer;
        // &:hover {
        //     background: var(--main-red-hover);

        // }
    }

    // .main-offer-btn:hover {
    //     opacity: 1;
    // }

    .Icon svg {
        vertical-align: bottom;
        margin-left: 1.5rem;
        margin-top: -7.5px;
    }

 `;

// Main title
export const Title = styled.h1`
    margin: 0 0 1.2rem;
    font-size: 4rem;
    font-weight: 700;
    line-height: 1.1em;
`;

// Subtitle
export const SubTitle = styled.h2`
    font-weight: 400;
    font-size: 1.4rem;
    line-height: 1.25em;
    margin: 0 0 1.875rem;
    text-transform: uppercase;
`;
