import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
    background: #000f22;
  }
`;

export const TabComponentStyle = styled.div`

    /* Tab Nav #1 */
    // body{
    //     background-color: #0000;
    // }

    .tab-icon-frontend {
        width: 2.8125rem;
        height: 2.8125rem;
        fill: var(--main-grey);
        margin-top: 10px;
    }

    .tab-icon-backend {
        width: 2.8125rem;
        height: 2.8125rem;
        fill: var(--main-grey);
        margin-top: 10px;
    }

    .tab-icon-database {
        width: 2.8125rem;
        height: 2.8125rem;
        fill: var(--main-grey);
        margin-top: 10px;
    }

    /* Tabs */
    // .tabs {
    //     padding-top: 1rem;
    // }

    .tab-nav-container {
        display: grid;
        justify-content: center;
        align-content: center;
        text-align: center;
        grid-template-columns: repeat(3, 1fr);
        grid-gap: 1rem;
        list-style: none;
        color: var(--main-grey);
        margin: 0 12%;
    }

    /* Tab Nav Hover Effect */

    ul li:hover {
        color: #FFF;
        cursor: pointer;
        opacity: 1;
        background: #000b19;
    }


    ul li:hover svg path {
        fill: #FFF;
    }

    /* The Active Tab */
    .active {
        border-bottom: 4px solid #2c83ba;
        
    }

`;