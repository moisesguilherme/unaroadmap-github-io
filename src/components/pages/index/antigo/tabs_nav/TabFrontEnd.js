import React from 'react'
import frontIcon from '../../../../assets/imgs/frontend_white.svg';

export default function TabFrontEnd() {
    return (
           <img src={frontIcon}  className="tab-icon-frontend"/> 
    )
}
