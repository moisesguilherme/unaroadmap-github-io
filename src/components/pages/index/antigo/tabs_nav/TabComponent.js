import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import TabFrontEnd from './TabFrontEnd';
import TabBackEnd from './TabBackEnd';
import TabDataBase from './TabDataBase';
import Scroll from 'react-scroll';

// Tabs Content
import TabContentOne from './TabContentOne';
import TabContentTwo from './TabContentTwo';
import TabContentThree from './TabContentThree';

//CSS
import { TabComponentStyle, GlobalStyle } from '../TabsNavStyle';
import { TabContent, Button } from 'reactstrap';

var Link = Scroll.Link;
var DirectLink = Scroll.DirectLink;
var Element = Scroll.Element;
var Events = Scroll.Events;
var scroll = Scroll.animateScroll;
var scrollSpy = Scroll.scrollSpy;
const styles = {
  fontFamily: 'sans-serif',
  textAlign: 'center',
};

class TabComponent extends Component {
    


    state = {
        tabIndex: 0
    }
    
    constructor(props) {
        super(props);
        this.scrollToTop = this.scrollToTop.bind(this);
      }
    
      componentDidMount() {
    
        Events.scrollEvent.register('begin', function () {
          console.log("begin", arguments);
        });
    
        Events.scrollEvent.register('end', function () {
          console.log("end", arguments);
        });
    
        scrollSpy.update();
    
      }
      scrollToTop() {
        scroll.scrollToTop();
      }
      componentWillUnmount() {
        Events.scrollEvent.remove('begin');
        Events.scrollEvent.remove('end');
      }
      
    render() {


        return (

            <>
                <GlobalStyle />
                {/* <Link activeClass="active" className="test1" to="test1" spy={true} smooth={true} duration={500} >teste</Link> */}
                <TabComponentStyle>
                    <Tabs
                        className="tabs"
                        selecteIndex={this.state.tabIndex}
                        onSelect={tabIndex => this.setState({ tabIndex })}
                    >
                      <Element name="test"></Element>
                        <TabList className="tab-nav-container">
                        
                            <Tab className={`${this.state.tabIndex === 0 ? 'tab-selected active' : null}`} onClick={() => scroll.scrollTo(800)} >
                                <TabFrontEnd />                                
                                <p style={{ marginTop: '1.5rem' }}><strong>Um projeto social que visa<br />
                             conectar, empresas e pessoas.</strong></p>

                            </Tab>
                        
                            <Tab className={`${this.state.tabIndex === 1 ? 'tab-selected active' : null}`} onClick={() => scroll.scrollTo(800)}>
                                <TabBackEnd />
                                <p style={{ marginTop: '1.5rem' }}>
                                    <strong>Empresa + Projetos Reais<br />+ Vagas de Oportunidades.</strong></p>
                            </Tab>

                            <Tab className={`${this.state.tabIndex === 2 ? 'tab-selected active' : null}`} onClick={() => scroll.scrollTo(800)}>
                                <TabDataBase />
                                <p style={{ marginTop: '2rem' }}><strong>Algumas áreas de atuação.</strong></p>
                            </Tab>
                        </TabList>
                        {/* Tab Content */}
                        <TabPanel>
                            <TabContentOne />
                        </TabPanel>
                        <TabPanel>
                            <TabContentTwo />
                        </TabPanel>
                        <TabPanel>
                            <TabContentThree />
                        </TabPanel>
                    </Tabs>
                </TabComponentStyle>
            </>
        )
    }
}

export default TabComponent;