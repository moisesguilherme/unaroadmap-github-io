import React from 'react';
import Img from '../../../../assets/imgs/tab1.jpg';
import styled from 'styled-components';
import { Button } from './Button';


function TabContentOne() {
    return (
        <TabContentContainer>
            <div className="container">
                <div className="tab-content">
                    <div>

                        <span style={{ marginBottom: '2rem', fontSize: '1.5rem'}}>
                           <h3>Trazemos através de projetos reais, conteúdos disponibilizados em <strong>Trilhas de Aprendizagem</strong>, com objetivo direto ao mercado de trabalho. Acesso totalmente gratuito em níveis iniciante, 
            intermediário e avançado.</h3>
                        </span>
                        {/* <br />
                        <Button style={{ marginTop: '2rem' }}>Registre-se</Button> */}
                    </div>
                    <img src={Img} />
                </div>
            </div>
        </TabContentContainer>
    )
}

export default TabContentOne;


// Main Content Container
const TabContentContainer = styled.div`
    background: #000b19;
    color: #fff;
    height: 85vh;

    .container{
        margin: 0 10%;
    }

    img {
        width: 31.875rem;
    }

    .tab-content {
        display: grid;
        grid-template-columns: repeat(1, 1fr);
        grid-gap: 2rem;
        align-items: center;
        font-size: 2rem;
        padding: 2.5rem;
    }

    @media (min-width: 645px) and (max-width: 940px) {
        height: 100vh;
    }
    
    @media (max-width: 644px) {
        height: 110vh;
    }

`;