import React from 'react';
import { Button } from './Button';
import styled from 'styled-components';
import ImgTv from '../../../../assets/imgs/tab-tv.png';
import ImgTablet from '../../../../assets/imgs/tab-tablet.png';
import ImgMacBook from '../../../../assets/imgs/tab-macbook.png';
import Img from '../../../../assets/imgs/tab2.jpg';

function TabContentTwo() {
    return (
        <TabContainer>
            <div className="tab-content">
                <div className="tab-top-content">
                    <span style={{fontSize: '1.5rem'}}>
                    Seja um profissional de tecnologia, estamos aqui para ajudar VOCÊ nesta conquista, para seu Futuro Profissional. 
VOCÊ pode construir aqui seu portfólio, e utilizar gratuitamente, ao final de cada oportunidade, 
você  já é direcionado para o processo seletivo, do projeto.
                    </span>
                    {/* <Button className="btn">Registre-se</Button> */}
                </div>
                <img src={Img} width="510" height="340"/>
                 {/* Tab Buttom Content
                <div className="tab-bottom-content">
                     TV Image Container 
                    <div>
                       <img src={ImgTv} style={{width: '18.75rem'}} />
                        <h3>Watch on your TV</h3>
                        <p>Smart TVs, PlayStation, Xbox, Chromecast, Apple TV, Blue-ray players and more.</p>
                    </div>

                     Tablet Image Container 
                    <div>
                        <img src={ImgTablet} style={{width: '18.75rem', paddingTop:'0.625rem'}} />
                        <h3>Watch on your TV</h3>
                        <p>Smart TVs, PlayStation, Xbox, Chromecast, Apple TV, Blue-ray players and more.</p>
                    </div>

                    TMac Image Container 
                    <div>
                        <img src={ImgMacBook} style={{width: '18.75rem', paddingTop: '0.625rem', paddingBottom: '0.625rem'}}/>
                        <h3>Watch on your TV</h3>
                        <p>Smart TVs, PlayStation, Xbox, Chromecast, Apple TV, Blue-ray players and more.</p>
                    </div>
                     
                </div>  */}
            </div>
        </TabContainer>
    )
}

export default TabContentTwo;

// Main Tab Content Container
const TabContainer = styled.div`
   background: #000b19;
   color: #fff;
   height: 85vh;

   .tab-content {
       margin: 0 15%;
   }

   //Tab Top Content
   .tab-top-content{
       display: grid;
       grid-template-columns: repeat(1, 1fr);
       justify-content: center;
       align-item: center;
       padding: 2.5rem 0;
   }

   span {
       grid-column: 1 / 8;
   }

   .btn  {
       margin:2 1.25rem 1.25rem;
       grid-column: 10 / 12;
   }

   // Tab Bottom Content
   .tab-bottom-content {
       display: grid;
       grid-template-columns: repeat(3, 1fr);
       grid-gap: 2rem;
       text-align: center;
       margin-top: 2rem;
   }

   h3 {
       margin: 0.5rem 0;
   }
   p {
       color: var(--main-white);
   }

   @media (min-width: 645px) and (max-width: 940px) {
        height: 100vh;
    }

    @media (max-width: 644px) {
        height: 110vh;
    }
`;
