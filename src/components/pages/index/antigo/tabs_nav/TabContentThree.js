import React from 'react'
import styled from 'styled-components';
import { Button } from './Button'
import { Icon } from 'react-icons-kit'
import {cross} from 'react-icons-kit/icomoon/cross'
import {checkmark} from 'react-icons-kit/icomoon/checkmark'
import Img from '../../../../assets/imgs/teste.jpg';


function TabContentThree() {
    return (
        <TabContainer>
            <div className="tab-content">
            <div className="tab-top-content">
                    <span style={{ marginBottom: '2rem', fontSize: '1.2rem'}}>
                    <strong>Front-end</strong><br/>
                    O desenvolvedor front-end é responsável  por “dar vida” à
                   interface gráfica de uma aplicação, um sistema web ou app.
                   <br/><br/>
                   <strong>Back-end</strong><br/>
                   O desenvolvedor Back-end é por cálculos, lógica de negócio, 
        interações de database e performance.de uma aplicação, um sistema web ou app.
        <br/><br/>
        <strong>Banco de Dados</strong><br/>
        O desenvolvedor DBA  é  responsável por gerenciar, instalar, configurar, atualizar e 
monitorar um banco de dados  ou sistemas de bancos de dados. 
                    </span>
                    {/* <Button className="btn">Registre-se</Button> */}
                </div>
                <img src={Img} width="510" height="240" />
                 {/* Tab Bottom Content */}
                {/* <div className="tab-bottom-content">
                    <table>
                        <hred>
                            <tr>
                                <th></th>
                                <th>Basic</th>
                                <th>Standard</th>
                                <th>Premium</th>
                            </tr>
                        </hred>
                        <tbody>
                            <tr>
                                <td>Teste1</td>
                                <td>Teste 2</td>
                                <td>Teste 3</td>
                                <td>Teste 4</td>
                            </tr>
                            <tr>
                                <td>Tste</td>
                                <td><Icon icon={cross} size={10}/></td>
                                <td><Icon icon={checkmark} size={10}/></td>
                                <td><Icon icon={checkmark} size={10}/></td>
                            </tr>
                        </tbody>
                    </table>
                </div> */}
            </div>
        </TabContainer>
    )
}

export default TabContentThree;

// Main Tab Content Container
const TabContainer = styled.div`
   background: #000b19;
   color: #fff;
   

   .tab-content {
       margin: 0 15%;
       padding-bottom: 1%;
       height: 85vh;
   }
  
   .tab-top-content {
        display: grid;
        grid-template-columns: repeat(1, 1fr);
        padding: 3rem 0 0;
    }

   span {
       grid-column: 3 / 9;
   }

   .btn {
       grid-column: 9 / 12;
       margin-left: 3rem;
       margin-right: 5.1rem; 
   }

   @media (min-width: 645px) and (max-width: 940px) {
    .tab-content {
            margin: 0 15%;
            padding-bottom: 1%;
            height: 100vh;
        }
   }

   @media (max-width: 644px) {
    .tab-content {
            margin: 0 15%;
            padding-bottom: 1%;
            height: 110vh;
        }
   }

//    @media (max-width: 719px) 
//         .tab-content {
//             margin: 0 15%;
//             padding-bottom: 1%;
//             height: 100vh;
//         }
//     }


   `;
