import React from 'react';
import {
  Container,
  Jumbotron
} from 'reactstrap';
import { FaProjectDiagram, FaTrophy, FaMapSigns} from 'react-icons/fa';
import bgImage1 from "./../../../../assets/imgs/empresa.jpg";
import bgImage2 from "./../../../../assets/imgs/crescimento.jpg";
import bgImage3 from "./../../../../assets/imgs/comunidade.jpg";
import 'bootstrap/dist/css/bootstrap.min.css';
import './estilo.css';

export const Sobre = () => (
  <Jumbotron fluid className="sobre">
    <Container className="text-center">
        <a name="sobre" className="sobre-link" />
        <div>
            <h2 className="display-4">Sobre Nós</h2>
            <p className="lead pb-4">Um pouco sobre nossas ideias</p>
        </div>
        <div className="row featurette">
            <div className="col-md-7">
                <h2 className="featurette-heading">Sua empresa é parte<span className="text-muted"> Fundamental</span> para nossa Plataforma.</h2>
                <p className="lead">Além de disponibilizar, oportunidades de trabalho através de projetos reais de sua demanda, estas mesmas oportunidades, 
                serão oferecidas para pessoas engajadas, comprometidas que iniciam e ou buscam uma nova oportunidade ao mercado de trabalho na área de tecnologia. 
                Assim, juntos, podemos contribuir mais e melhor na valorização de pessoas, ajudando-as a ampliar seus conhecimentos através da nossa plataforma.</p>
            </div>
            <div className="col-md-5">
                <img src={bgImage1} className="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" alt="Responsive_image_1" width="500" height="700"/>
            </div>
        </div>
        <hr className="featurette-divider"/>
        <div className="row featurette">
            <div className="col-md-7 order-md-2">
                <h2 className="featurette-heading">Nós nos<span className="text-muted"> importamos</span> com seu crescimento profissional.</h2>
                <p className="lead">Seja um profissional de tecnologia , Estamos aqui para ajudar no seu Futuro Profissional. Você pode construir aqui seu portfólio, 
                e utilizar gratuitamente, ao final de cada oportunidade, você já é direcionado para o processo seletivo, do projeto.</p>
            </div>
            <div className="col-md-5 order-md-1">
                <img src={bgImage2} className="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" alt="Responsive_image_1" width="500" height="700"/>
            </div>
        </div>
        <hr className="featurette-divider"/>
        <div className="row featurette">
            <div className="col-md-7">
                <h2 className="featurette-heading">Este é um projeto <span className="text-muted"> social</span> que visa <span className="text-muted"> conectar</span>, empresas e pessoas.</h2>
                <p className="lead">Oportunidades de vagas, em projetos reais em níveis: iniciante, intermediário e avançado, oferecidos às comunidades carentes que querem inicar na área de 
                Tecnologia de desenvolvimento e ou estão fora do mercado de trabalho. Oferencendo aprendizado com conteúdos de qualidade, totalmentes gratuitos, de acordo com os projetos cadastrados. 
                Trilhas para acesso à portais institucionais (universidades)</p>
            </div>
            <div className="col-md-5">
                <img src={bgImage3} className="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" alt="Responsive_image_1" width="500" height="700"/>
            </div>
        </div>
    </Container>
  </Jumbotron>
);