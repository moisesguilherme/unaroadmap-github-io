import React from 'react';
import Header from './../layouts/header/Header';
import {Principal} from './principal/Principal';
import {Servicos} from './servicos/Servicos';
import {Sobre} from './sobre/Sobre';
import {FooterBar} from './../layouts/footerBar/FooterBar';

export default (props) => {
    return(
        <div>
            <Header/>
            <Principal/>
            <Servicos/>
            <Sobre/>
            <FooterBar background="#000f22" color="white"></FooterBar>
        </div>  
    );
}


