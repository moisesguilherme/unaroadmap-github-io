import React, { useState } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

import api from '../../../../services/api';

import {
    Container,
    Jumbotron
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './estilo.css';

import ActiveProjectsCard from './ActiveProjectsCard';
import MyProjectsCard from './MyProjectsCard';


export default function ShowCarouselTrails(props) {


    const reloadComponent = props.callback;

    let MSG_NENHUMA = ''
    let dataProjects = props.projects.map === undefined ? '' : props.projects.map;
    let dataMyProjects = props.myprojects.map === undefined ? '' : props.myprojects.map;


    function candidateParticipateWithProject(project_id) {
        console.log(">>>>>> clicou em participar", project_id, props.candidate);
        registerParticipate(project_id, props.candidate.id);

    }


    async function registerParticipate(project_id, candidate_id) {
        console.log(">>>> registerParticipate", project_id, candidate_id);

        const response = await api.put(`projects/${project_id}`, { "candidate_id": candidate_id }).then((response) => {
            console.log(`
          --SALVOU--
          ${JSON.stringify(response.data)}
        `)

            reloadComponent();
          

        }, (error) => {
            console.log(">>>", error);
        })
    }


    let itensComponentProjects = '';
    let itensComponentMyProjects = '';


    if (dataProjects !== '') {
        itensComponentProjects = props.projects.map(item => (
            <ActiveProjectsCard key={item.id} data={item} callback_click={candidateParticipateWithProject} />
        ))
    }

    if (dataMyProjects !== '') {

        itensComponentMyProjects = props.myprojects.map(item => (
            <MyProjectsCard key={item.id} data={item} callback_click={candidateParticipateWithProject} />
        ))
        
        if( itensComponentMyProjects.length === 0){
            MSG_NENHUMA = "Vocë ainda não está participando de nenhum projeto."
        }        
    }

    return (
        <Jumbotron fluid className="trilhas">
            <Container className="text-center trail">
                <div className="row">
                    <div className="col-md-8">
                        <Carousel>
                            {itensComponentProjects}
                        </Carousel>
                    </div>
                    <div className="col-md-4">
                        <div>
                            <p className="lead pb-4">Minhas Trilhas.</p>
                        </div>
                            {(itensComponentMyProjects.length > 0) && <Carousel>{itensComponentMyProjects}</Carousel>}
                            {(itensComponentMyProjects.length == 0) && <p>{MSG_NENHUMA}</p>}

                    </div>
                </div>
            </Container>
        </Jumbotron>
    );
}





















// import React, { useState } from 'react';
// import Carousel from 'react-bootstrap/Carousel';
// import Img from "../../project/covers/html_css.jpg";
// import Img2 from "../../project/covers/html_css.jpg";
// import Img3 from "../../project/covers/html_css.jpg";
// import {
//       Container,
//       Jumbotron
//     } from 'reactstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import './estilo.css';




// export default function ListaTrilha() {
//     const [index, setIndex] = useState(0);

//   const handleSelect = (selectedIndex, e) => {
//     setIndex(selectedIndex);
//   };

//     return (
//         <Jumbotron fluid className="trilhas">
//             <Container className="text-center">
//                <Carousel activeIndex={index} onSelect={handleSelect}>
//                     <Carousel.Item>
//                         <img
//                             className="d-block w-100"
//                             src={Img}
//                             alt="First slide"
//                             width="500" 
//                             height="700"
//                         />
//                         <Carousel.Caption center>
//                             <h3>First slide label</h3>
//                             <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
//                         </Carousel.Caption>
//                     </Carousel.Item>
//       <Carousel.Item>
//         <img
//           className="d-block w-100"
//           src={Img2}
//           alt="Second slide"
//           width="500" 
//           height="700"
//         />

//         <Carousel.Caption>
//           <h3>Second slide label</h3>
//           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
//         </Carousel.Caption>
//       </Carousel.Item>
//       <Carousel.Item>
//         <img
//           className="d-block w-100"
//           src={Img3}
//           alt="Third slide"
//           width="500" 
//           height="700"
//         />

//         <Carousel.Caption>
//           <h3>Third slide label</h3>
//           <p>
//             Praesent commodo cursus magna, vel scelerisque nisl consectetur.
//           </p>
//         </Carousel.Caption>
//       </Carousel.Item>
//     </Carousel>
//             </Container>
//         </Jumbotron>
//     );
// }