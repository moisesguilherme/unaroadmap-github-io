import React, { useState, useEffect } from "react";

const MiniCard = (props) => {
        
    console.log(">>>> data", props.data);

    let id = props.data === undefined ? '' : props.data.id;
    let name = props.data === undefined ? '' : props.data.name;
    let description = props.data === undefined ? '' : props.data.description;
    let logo = props.data === undefined ? '' : props.data.logo;
    if(logo === null) logo = "black_empty_card.jpg";
    
    return (
        <div>
        <img src={require('../../project/covers/' + logo )} className="img-slide"/>
        <p className="legend" id="botao1">
            {name} <br/>{description}<br/>
            <a className="btn btn-info" href={`/project/trail?project=${id}`} role="button">Abrir</a>
        </p>
    </div>
    )
}

export default MiniCard