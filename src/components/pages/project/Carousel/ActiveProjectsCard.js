import React, { useState, useEffect } from "react";

import { CardBody } from "reactstrap";

const ProjectsCard = (props) => {
    
    console.log(">>>> data", props.data);
    const cb = props.callback_click;
    
    let id = props.data === undefined ? '' : props.data.id;
    let name = props.data === undefined ? '' : props.data.name;
    let description = props.data === undefined ? '' : props.data.description;
    let logo = props.data === undefined ? '' : props.data.logo;
    if(logo === null) logo = "black_empty_card.jpg";

    return (
        <div>
        <img src={ require('../../project/covers/' + logo ) } className="img-slide"/>
        <div className="legend" id="botao1">
            <p className="legend" id="botao1" style={{fontSize: 15 + 'px'}}>
                {name}<br/>
                {description}
            </p>
            <a className="btn btn-info" href={`/project/trail?project=${id}`} role="button">Visualizar</a>
            <a className="btn btn-info" href="#" role="button" onClick={ _ => {cb(id)}}>Participar</a>
        </div>
    </div>
    )
}

export default ProjectsCard