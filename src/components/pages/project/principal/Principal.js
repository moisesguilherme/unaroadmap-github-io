import React from 'react';
import {
  Container,
  Jumbotron
} from 'reactstrap';
import { FaProjectDiagram, FaTrophy, FaMapSigns} from 'react-icons/fa';
//import '../Carousel/node_modules/bootstrap/dist/css/bootstrap.min.css';
import './estilo.css';

export const Principal = (props) => (
  <Jumbotron fluid className="menu-projeto">
    <Container className="text-center">
        <div>
            <h2 className="display-4">Olá {props.usuario}!</h2>
            <p className="lead pb-4">Seja bem vindo à UnaRoadMap!</p>
        </div>  
    </Container>
  </Jumbotron>
);