import React, { useState, useEffect } from "react";
import ListaTrilha from "./Carousel/ShowCarouselTrails";
import {Principal} from './principal/Principal';
import {
  Jumbotron,
  Button,
  Container,
  Row,
  CardGroup,
  Card,
} from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import api from '../../../services/api';

import { FooterBar } from "../layouts/footerBar/FooterBar";
import { ContainerPrincipal, ContainerFooter } from "./styles";
import SideBar from "../layouts/Sidebar/Sidebar";
//import SideBarEmpresa from '../layouts/Sidebar/SidebarEmpresa'

import MenuProject from "../layouts/DropDowMenu/DropDownMenu";

import "./App.css";

//Redux dependencies
import { connect } from "react-redux";
import  store  from "../../../store"

const Project = () => {
  const history = useHistory();
  
  const [init, setInit] = useState(false);
  const [userName, setUserName] = useState('');
  const [candidate, setCandiate] = useState('');
  const [dataProjetcts, setDataProjects] = useState('');
  const [dataMyProjects, setDataMyProjects] = useState('');
  

  useEffect(() => {
    console.log(">>> Atualizou a página")
    initilize();
  }, [init])
 
 const user_id  = localStorage.getItem("user_id");

  function initilize() {
    let user_id = localStorage.getItem("user_id"); 
    
    if(user_id === undefined || user_id === null){
      console.log("Sem user id");
      localStorage.removeItem("user_id");
      history.push('/');
    }else{
      ///const user_id = 1091
      console.log(">>> id do usuário", user_id)
      getNameCanditate(user_id);
      getProjects();
      getProjectsByUser(user_id)
    }
  }

 
  async function getNameCanditate(user_id) {
    const response = await api.get(`candidates/user/${user_id}`).then((response) => {
      console.log("#1 dados do Candidato", response.data)
      try{
        setUserName(response.data.name.split(' ')[0]); //Pega só o primeiro nome
        setCandiate(response.data);
      }catch(err){
        console.log(">>> cadidato não registrado");
        history.push('/');
      }
      
    }, (error) => {
      console.log(">>>", error);
    })
  }
  

  async function getProjects() {
    const response = await api.get(`projects`).then((response) => {
      console.log("#2 pega todos projetos aberto", response.data);
      setDataProjects(response.data);
    }, (error) => {
      console.log(">>>", error);
    })
  }
  
  async function getProjectsByUser() {
    console.log(">>>>>>>>> chamou")
    const user_id =  localStorage.getItem("user_id");
    console.log(">>>> get Project by user:", `/users/${user_id}/projects`)

    const response = await api.get(`/users/${user_id}/projects`).then((response) => {
      console.log("#2 pega todos projetos do usuário: " + user_id, response.data);
      setDataMyProjects(response.data);
    }, (error) => {
      console.log(">>>", error);
    })
  }

  return (
      <>
        <MenuProject />
        <Principal usuario={userName} />
        <ListaTrilha projects={dataProjetcts} myprojects={dataMyProjects} candidate={candidate} callback={getProjectsByUser}/>
      </>
  );

  }

  
export default Project;
