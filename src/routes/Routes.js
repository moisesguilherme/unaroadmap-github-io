import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import { isAuthenticated } from './../services/auth';

import Index from './../components/pages/index/Index';
import Login from './../components/pages/login/Login';
import Forgot from './../components/pages/login/Forgot';
import NewPassword from './../components/pages/login/NewPassword';
import NotFound from './../components/pages/notFound/NotFound';
import Project from "./../components/pages/project/Project";
import RegisterUser from '../components/pages/register/RegisterUser';
import RegisterCandidate from '../components/pages/register/RegisterCandidate'
import RegisterCompany from '../components/pages/register/RegisterCompany'
import RegisterAddress from '../components/pages/register/FormAddress'
import Trail from '../components/pages/trail';
import WbnPlayer from '../components/pages/videoPlayer/containers/WbnPlayer';
import LoadPlayer from '../components/pages/videoPlayer/containers/LoadPlayer';

import AdminHome from './../components/pages/admin/home/Home';
import AdminUser from './../components/pages/admin/user/UserCrud';
import AdminCandidade from './../components/pages/admin/user/CandidateCrud';
import AdminTrail from './../components/pages/admin/trail/Trail';

const PrivateRoute = ({ component: Component, ...rest }) => (
 <Route 
   {...rest}
   render={props =>
    isAuthenticated() ? (
      <Component {...props} />
    ) : (
      <Redirect to={{ pathname: "/", state: { from: props.location } }} />
    )
  }
  />
);

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path='/' component={Index} />
      <Route exact path="/login" component={Login}/>
      <Route path="/login/forgot" component={Forgot}/>
      <Route path="/register/user" component={RegisterUser}/>
      <Route path="/login/newpassword" component={NewPassword}/>
      <PrivateRoute path="/loadplayer" component={LoadPlayer} />
      <PrivateRoute path="/videoplayer/:activeVideo" component={WbnPlayer} />
      <PrivateRoute path="/register/candidate" component={RegisterCandidate}/>
      <PrivateRoute path="/register/company" component={RegisterCompany}/>
      <PrivateRoute exact path="/project" component={Project}/>
      <PrivateRoute path="/project/trail" component={Trail} />   
      <PrivateRoute path="/register/address" component={RegisterAddress}/>
      <PrivateRoute exact path='/admin' component={AdminHome} />
      <PrivateRoute path='/admin/company' component={AdminHome} />
      <PrivateRoute path='/admin/users' component={AdminUser} />
      <PrivateRoute path='/admin/candidates' component={AdminCandidade} />
      <PrivateRoute path='/admin/trails' component={AdminTrail} />
      <PrivateRoute path="*" component={NotFound}/>
    </Switch>
  </BrowserRouter>
);

export default Routes;