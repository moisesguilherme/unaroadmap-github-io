import axios from "axios";

const api = axios.create({
    baseURL: 'https://unaroadmapsendmail.herokuapp.com/'
});


export default api;