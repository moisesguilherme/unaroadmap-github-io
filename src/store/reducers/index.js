import { combineReducers } from 'redux';

import login from './_login';

export default combineReducers({
    login
});

