export const loginSuccess = id => ({
    type: 'LOGIN_SUCCESS',
    playload: { id },
});

export const loginFail = text => ({
    type: 'LOGIN_FAIL',
    playload: { text },
})

export const logOut = id => ({
    type: 'LOGOUT',
    playload: { id },
})

export const loginWithRegister = (id, document_id) => ({
    type: 'LOGIN_WITH_REGISTER',
    playload: { id, document_id },
})
