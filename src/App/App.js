import React from 'react';
import '../config/ReactotronConfig';
//import '../styles/App.css';
import '../styles/global';
import Routes from './../routes/Routes';
import { Provider } from 'react-redux';
import store from '../store';

//console.tron.log({ hello: 'world'})

export default (props) => {
  return (
      <Provider store={store}>
        <Routes></Routes>
      </Provider>
  );
};
